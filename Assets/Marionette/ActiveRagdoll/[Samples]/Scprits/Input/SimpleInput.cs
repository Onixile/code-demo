using Marionette.ActiveRagdoll.Runtime._Interfaces;
using UnityEngine;

namespace Marionette.ActiveRagdoll._Samples_._General_.Scprits.Input
{
  public class SimpleInput : IInput
  {
    public bool AxisIsZero() => 
      GetAxis() == Vector2.zero;

    public Vector2 GetAxis() => new Vector2(
      UnityEngine.Input.GetAxis("Horizontal"),
      UnityEngine.Input.GetAxis("Vertical"));

    public bool GetKey(KeyCode keyCode) =>
      UnityEngine.Input.GetKey(keyCode);

    public bool GetKeyDown(KeyCode keyCode) =>
      UnityEngine.Input.GetKeyDown(keyCode);

    public bool GetKeyUp(KeyCode keyCode) =>
      UnityEngine.Input.GetKeyUp(keyCode);
  }
}