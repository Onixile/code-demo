using System;
using Marionette.ActiveRagdoll.Runtime.Animation._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Joint;

namespace Marionette.ActiveRagdoll._Samples_._Simple_Without_Animation_Demo_.Initializer
{
  public class WithoutAnimationMusclesController: IMusclesController
  {
    public event Action<bool> OnSetActiveMuscles;

    public void Update()
    {
    }

    public void LateUpdate()
    {
    }

    public void FixedUpdate()
    {
    }

    public void OnDrawGizmos()
    {
    }

    public void SetActiveMuscles(bool value)
    {
    }

    public void SetMuscleToDefault(JointType jointType)
    {
    }

    public void SetMuscle(JointType jointType, float spring, float damper)
    {
    }
  }
}