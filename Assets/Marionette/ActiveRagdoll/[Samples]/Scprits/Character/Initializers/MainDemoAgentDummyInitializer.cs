using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.Simple.Agent;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine;
using Marionette.ActiveRagdoll.Runtime._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Animation._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Initializer;
using Marionette.ActiveRagdoll.Runtime.Joint;

namespace Marionette.ActiveRagdoll._Samples_._Main_Demo_.Initializer
{
  public class MainDemoAgentDummyInitializer : ActiveRagdollInitializer
  {
    public override ICharacterController InitializeCharacterController(IInput input, ICamera camera, StateConfig stateConfig,
      IAnimationController animationController, JointDatasContainer jointsContainer) =>
      new SimpleAgentDummyCharacterController(animationController, stateConfig, jointsContainer);

    public override void SetupCamera(out ICamera camera, JointDatasContainer jointDatasContainer) =>
      camera = null;
  }
}