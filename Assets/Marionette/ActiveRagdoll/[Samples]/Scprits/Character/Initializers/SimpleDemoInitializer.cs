using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.Simple.Player;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine;
using Marionette.ActiveRagdoll.Runtime._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Animation._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Initializer;
using Marionette.ActiveRagdoll.Runtime.Joint;

namespace Marionette.ActiveRagdoll._Samples_._Simple_Demo_.Initializer
{
  public class SimpleDemoInitializer : ActiveRagdollInitializer
  {
    public override ICharacterController InitializeCharacterController(IInput input, ICamera camera, StateConfig stateConfig, 
      IAnimationController animationController, JointDatasContainer jointsContainer) =>
      new SimplePlayerCharacterController(input, camera, animationController, stateConfig, jointsContainer);
  }
}
