using System;
using System.Collections.Generic;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers._MotionExecutors_._Interfaces;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers._MotionExecutors_.DirectionCalculators;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.Behaviour.LB;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.Behaviour.UB;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.Checker;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.Condition;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.StateMachines;
using Marionette.ActiveRagdoll.Runtime._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Animation._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Initializer;
using Marionette.ActiveRagdoll.Runtime.Joint;

namespace Marionette.ActiveRagdoll._Samples_._Main_Demo_.Initializer
{
  public class MainDemoInitializer : ActiveRagdollInitializer
  {
    public override ICharacterController InitializeCharacterController(IInput input, ICamera camera, StateConfig stateConfig,
      IAnimationController animationController, JointDatasContainer jointsContainer)
    {
      IMotionDirectionCalculator motionDirectionCalculator = new InputMotionDirectionCalculator(input, camera);

      SetupCheckerStates(out Dictionary<Type, IUpdatableState> checkerStates, stateConfig, jointsContainer);
      SetupUpperBodyBehaviourStates(out BehaviourStateMachine upperBodyBehaviourStates, input, camera, stateConfig, jointsContainer, animationController, checkerStates);
      SetupLowerBodyBehaviourStates(out BehaviourStateMachine lowerBodyBehaviourStates, motionDirectionCalculator, input, stateConfig, animationController, jointsContainer, checkerStates);
      SetupConditionStateMachine(out ConditionStateMachine conditionStates, input, stateConfig, animationController, upperBodyBehaviourStates, lowerBodyBehaviourStates, checkerStates);

      return new StatesCharacterController(checkerStates, upperBodyBehaviourStates, lowerBodyBehaviourStates, conditionStates);
    }

    protected virtual void SetupCheckerStates(out Dictionary<Type, IUpdatableState> checkerStates,
      StateConfig config, JointDatasContainer jointsContainer)
    {
      checkerStates = new Dictionary<Type, IUpdatableState>();
      checkerStates.Add(typeof(Grounded), new Grounded(config, checkerStates, jointsContainer));
    }

    protected virtual void SetupUpperBodyBehaviourStates(out BehaviourStateMachine upperBodyBehaviourStates,
      IInput input, ICamera camera, StateConfig config, JointDatasContainer jointsContainer,
      IAnimationController animationController, Dictionary<Type, IUpdatableState> updatableStates)
    {
      upperBodyBehaviourStates = new BehaviourStateMachine();
      upperBodyBehaviourStates.SetStates<Idle>(new Dictionary<Type, IExitableState>
      {
        [typeof(Idle)] = new Idle(config, input, animationController, upperBodyBehaviourStates, updatableStates),
        [typeof(Attack)] = new Attack(config, input, animationController, upperBodyBehaviourStates, updatableStates),
        [typeof(Grab)] = new Grab(config, input, camera, animationController, upperBodyBehaviourStates, updatableStates, jointsContainer),
        [typeof(Crawl)] = new Crawl(config, input, animationController, upperBodyBehaviourStates, updatableStates),
      });
    }

    protected virtual void SetupLowerBodyBehaviourStates(out BehaviourStateMachine lowerBodyBehaviourStates,
      IMotionDirectionCalculator motionDirectionCalculator, IInput input, StateConfig config,
      IAnimationController animationController, JointDatasContainer jointDatasContainer, Dictionary<Type, IUpdatableState> checkerStates)
    {
      lowerBodyBehaviourStates = new BehaviourStateMachine();
      lowerBodyBehaviourStates.SetStates<Steer>(new Dictionary<Type, IExitableState>
      {
        [typeof(Steer)] = new Steer(config, motionDirectionCalculator, input, animationController, lowerBodyBehaviourStates, jointDatasContainer, checkerStates),
        [typeof(Jump)] = new Jump(config, input, animationController, lowerBodyBehaviourStates, jointDatasContainer, checkerStates),
      });
    }

    protected virtual void SetupConditionStateMachine(out ConditionStateMachine conditionStates, IInput input, StateConfig config, IAnimationController animationController,
      BehaviourStateMachine upperBodyBehaviourStates, BehaviourStateMachine lowerBodyBehaviourStates, Dictionary<Type, IUpdatableState> checkerStates)
    {
      conditionStates = new ConditionStateMachine();
      conditionStates.SetStates<Active>(new Dictionary<Type, IExitableState>
      {
        [typeof(Active)] = new Active(config, input, animationController, upperBodyBehaviourStates, lowerBodyBehaviourStates, conditionStates, checkerStates),
        [typeof(Knockout)] = new Knockout(config, animationController, upperBodyBehaviourStates, lowerBodyBehaviourStates, conditionStates, checkerStates),
      });
    }
  }
}