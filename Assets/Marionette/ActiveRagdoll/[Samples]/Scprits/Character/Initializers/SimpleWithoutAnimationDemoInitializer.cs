using System.Collections.Generic;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.Simple.Player;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine;
using Marionette.ActiveRagdoll.Runtime._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Animation;
using Marionette.ActiveRagdoll.Runtime.Animation._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Animation.Muscle;
using Marionette.ActiveRagdoll.Runtime.Equipment._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Initializer;
using Marionette.ActiveRagdoll.Runtime.Joint;
using UnityEngine;

namespace Marionette.ActiveRagdoll._Samples_._Simple_Without_Animation_Demo_.Initializer
{
  public class SimpleWithoutAnimationDemoInitializer : ActiveRagdollInitializer
  {
    public override ICharacterController InitializeCharacterController(IInput input, ICamera camera, StateConfig stateConfig,
      IAnimationController animationController, JointDatasContainer jointsContainer) =>
      new SimplePlayerWithoutAnimationCharacterController(input, camera, animationController, stateConfig, jointsContainer);

    public override IAnimationController InitializeAnimationController(Runtime.ActiveRagdoll owner, AnimationConfig animationConfig, Animator animator,
      JointDatasContainer jointDatasContainer, AnimationControllingPointsContainer inverseKinematicsPointsContainer, ISelfCollisionController collisionController, IEquipmentController equipmentController)
    {
      DestroyImmediate(animator);

      JointData[] jointDatas = jointDatasContainer.GetAll();
      IMusclesController musclesController = new WithoutAnimationMusclesController();

      SetupInverseKinematicsLimbControllers(out List<IInverseKinematicsLimbController> inverseKinematicsLimbControllers,
        inverseKinematicsPointsContainer, jointDatas);
      SetupInverseKinematicsLookAtController(out IInverseKinematicsLookAtController inverseKinematicsLookAtController, inverseKinematicsPointsContainer, jointDatas);

      MusclesAnimation musclesAnimation = inverseKinematicsPointsContainer.GetComponent<MusclesAnimation>();
      musclesAnimation.Construct(animationConfig);
      
      return new AnimationController(jointDatasContainer, animationConfig, animator,
        inverseKinematicsLimbControllers, inverseKinematicsLookAtController, null, musclesController,
        null, null, null, null, null);
    }
  }
}