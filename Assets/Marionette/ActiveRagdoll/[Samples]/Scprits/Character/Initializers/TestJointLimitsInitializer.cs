using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine;
using Marionette.ActiveRagdoll.Runtime._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Animation;
using Marionette.ActiveRagdoll.Runtime.Animation._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Equipment._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Initializer;
using Marionette.ActiveRagdoll.Runtime.Joint;
using UnityEngine;

namespace Marionette.ActiveRagdoll._Samples_._Test_Joint_Limits_Demo_.Initializer
{
  public class TestJointLimitsInitializer : ActiveRagdollInitializer
  {
    public override ICharacterController InitializeCharacterController(IInput input, ICamera camera, StateConfig stateConfig,
      IAnimationController animationController, JointDatasContainer jointsContainer)
    {
      Rigidbody rigidbody = jointsContainer.Get(JointType.Pelvis).Rigidbody;
      rigidbody.constraints = RigidbodyConstraints.FreezeAll;

      foreach (JointData jointData in jointsContainer.GetAll())
        if (jointData.Joint)
          jointData.Transform.gameObject.AddComponent<ConfigurableJointTestRotationLimits>();

      return null;
    }

    public override IAnimationController InitializeAnimationController(Runtime.ActiveRagdoll owner, AnimationConfig animationConfig, Animator animator,
      JointDatasContainer jointDatasContainer, AnimationControllingPointsContainer inverseKinematicsPointsContainer,
      ISelfCollisionController collisionController, IEquipmentController equipmentController) =>
      null;
  }
}