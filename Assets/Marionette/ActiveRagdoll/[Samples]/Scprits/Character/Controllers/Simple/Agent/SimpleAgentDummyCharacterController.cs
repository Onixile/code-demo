using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers._MotionExecutors_.DirectionCalculators;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine;
using Marionette.ActiveRagdoll.Runtime.Animation._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Joint;
using UnityEngine;

namespace Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.Simple.Agent
{
  public class SimpleAgentDummyCharacterController : SimpleCharacterController
  {
    private const float MovementSensitivity = 1f;

    private readonly int _animationLayer;
    private readonly Rigidbody _root;

    public SimpleAgentDummyCharacterController(IAnimationController animationController, StateConfig stateConfig, JointDatasContainer joints)
      : base(animationController, stateConfig, joints, new ZeroMotionDirectionCalculator())
    {
      _animationLayer = animationController.Animator.GetLayerIndex(stateConfig.LowerBody.AnimationLayer);
      _root = joints.Get(JointType.Pelvis).Rigidbody;

      SleepPositionMotion(true);
      SleepRotationMotion(true);
    }

    public override void Update()
    {
      base.Update();
      
      SwitchAnimation();
    }

    private void SwitchAnimation()
    {
      if (CheckAnimation(_stateConfig.LowerBody.Steering.Move.EnterAnimState, _animationLayer))
        return;

      PlayAnimation(_root.velocity.magnitude > MovementSensitivity
        ? _stateConfig.LowerBody.Steering.Move.ActionAnimState
        : _stateConfig.LowerBody.Steering.Move.IdleAnimState, _animationLayer);
    }
  }
}