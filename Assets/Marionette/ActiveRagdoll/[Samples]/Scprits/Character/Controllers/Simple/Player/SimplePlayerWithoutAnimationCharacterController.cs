using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers._MotionExecutors_.DirectionCalculators;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine;
using Marionette.ActiveRagdoll.Runtime._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Animation._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Joint;

namespace Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.Simple.Player
{
  public class SimplePlayerWithoutAnimationCharacterController : SimpleCharacterController
  {
    public SimplePlayerWithoutAnimationCharacterController(IInput input, ICamera camera, IAnimationController animationController,
      StateConfig stateConfig, JointDatasContainer joints)
      : base(animationController, stateConfig, joints, new InputMotionDirectionCalculator(input, camera))
    {
    }
  }
}