using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers._MotionExecutors_.DirectionCalculators;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine;
using Marionette.ActiveRagdoll.Runtime._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Animation._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Joint;
using UnityEngine;

namespace Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.Simple.Player
{
  public class SimplePlayerCharacterController : SimpleCharacterController
  {
    private readonly IInput _input;

    private readonly Rigidbody _root;

    private readonly int _UBAnimationLayer;
    private readonly int _LBAnimationLayer;

    private bool _lastAttackArmIsRight;

    public SimplePlayerCharacterController(IInput input, ICamera camera, IAnimationController animationController,
      StateConfig stateConfig, JointDatasContainer joints)
      : base(animationController, stateConfig, joints, new InputMotionDirectionCalculator(input, camera))
    {
      _input = input;

      _root = joints.Get(JointType.Pelvis).Rigidbody;

      _UBAnimationLayer = animationController.Animator.GetLayerIndex(stateConfig.UpperBody.AnimationLayer);
      _LBAnimationLayer = animationController.Animator.GetLayerIndex(stateConfig.LowerBody.AnimationLayer);
    }

    public override void Update()
    {
      base.Update();

      DoIdleState();
      DoAttackState();
      DoGrabState();

      DoCrawlState();

      DoMoveState();
      DoJumpState();
      
      SwitchLookAt();
    }

    private void DoIdleState()
    {
      if (_input.GetKeyUp(KeyCode.E) || _input.GetKeyUp(KeyCode.Q))
      {
        if (!CheckAnimation(_stateConfig.UpperBody.Idle.IdleAnimState, _UBAnimationLayer))
          PlayAnimation(_stateConfig.UpperBody.Idle.IdleAnimState, _UBAnimationLayer);

        DeactivateConnectionController();
        SwitchChangeSolversIterations(false);
      }
    }

    private void DoAttackState()
    {
      if (!_lastAttackArmIsRight && CheckAnimation(_stateConfig.UpperBody.Attack.IdleAnimState, _UBAnimationLayer) && _input.GetKey(KeyCode.Mouse0))
      {
        PlayAnimation(_stateConfig.UpperBody.Attack.RightAttackAnimState, _UBAnimationLayer);
        _lastAttackArmIsRight = true;
      }
      else if (_lastAttackArmIsRight && CheckAnimation(_stateConfig.UpperBody.Attack.IdleAnimState, _UBAnimationLayer) && _input.GetKey(KeyCode.Mouse0))
      {
        PlayAnimation(_stateConfig.UpperBody.Attack.LeftAttackAnimState, _UBAnimationLayer);
        _lastAttackArmIsRight = false;
      }

      AddTemporaryConnection();

      void AddTemporaryConnection()
      {
        if (CheckAnimation(_stateConfig.UpperBody.Attack.RightAttackAnimState, _UBAnimationLayer) | CheckAnimation(_stateConfig.UpperBody.Attack.LeftAttackAnimState, _UBAnimationLayer))
        {
          AnimatorStateInfo animationState = GetCurrentAnimatorStateInfo(_UBAnimationLayer);
          AnimatorClipInfo[] myAnimatorClip = GetCurrentAnimatorClipInfo(_UBAnimationLayer);

          if (myAnimatorClip.Length == 0)
            return;

          float time = animationState.normalizedTime / myAnimatorClip[0].clip.length;

          if (time > _stateConfig.UpperBody.Attack.ConnectionStart && time < _stateConfig.UpperBody.Attack.ConnectionFinish)
            ActivateConnectionController(_stateConfig.UpperBody.Attack);
          else
            DeactivateConnectionController();
        }
      }
    }

    private void DoGrabState()
    {
      if (_input.GetKeyDown(KeyCode.E))
      {
        if (CheckAnimation(_stateConfig.UpperBody.Grab.IdleAnimState, _UBAnimationLayer))
          PlayAnimation(_stateConfig.UpperBody.Grab.GrabAnimState, _UBAnimationLayer);

        ActivateConnectionController(_stateConfig.UpperBody.Grab);
        SwitchChangeSolversIterations(true);
      }
    }

    private void DoCrawlState()
    {
      if (_input.GetKey(KeyCode.Q) && CheckGrounded())
      {
        if (!_input.AxisIsZero())
        {
          if (!CheckAnimation(_stateConfig.UpperBody.Crawl.CrawlAnimState, _UBAnimationLayer))
            PlayAnimation(_stateConfig.UpperBody.Crawl.CrawlAnimState, _UBAnimationLayer);
        }
        else
          PlayAnimation(_stateConfig.UpperBody.Crawl.IdleAnimState, _UBAnimationLayer);

        SetSteeringData(_stateConfig.LowerBody.Steering.Crawl);
        
        if (!CheckAnimation(_stateConfig.LowerBody.Steering.Crawl.IdleAnimState, _LBAnimationLayer) || !CheckAnimation(_stateConfig.LowerBody.Steering.Crawl.ActionAnimState, _LBAnimationLayer))
          PlayAnimation(_stateConfig.LowerBody.Steering.Crawl.EnterAnimState, _LBAnimationLayer);
        
        if (!CheckAnimation(_stateConfig.LowerBody.Steering.Crawl.IdleAnimState, _LBAnimationLayer) && !CheckAnimation(_stateConfig.LowerBody.Steering.Crawl.ActionAnimState, _LBAnimationLayer))
          return;

        PlayAnimation(!_input.AxisIsZero()
            ? _stateConfig.LowerBody.Steering.Crawl.ActionAnimState
            : _stateConfig.LowerBody.Steering.Crawl.IdleAnimState,
          _LBAnimationLayer);
      }
    }

    private void DoMoveState()
    {
      if(_input.GetKeyUp(KeyCode.Q))
        SetSteeringData(_stateConfig.LowerBody.Steering.Move);
      
      if (CheckAnimation(_stateConfig.LowerBody.Steering.Move.EnterAnimState, _LBAnimationLayer))
        return;

      PlayAnimation(!_input.AxisIsZero()
          ? _stateConfig.LowerBody.Steering.Move.ActionAnimState
          : _stateConfig.LowerBody.Steering.Move.IdleAnimState,
        _LBAnimationLayer);
    }

    private void DoJumpState()
    {
      if (_input.GetKeyDown(KeyCode.Space))
      {
        if (CheckGrounded())
        {
          if (_stateConfig.LowerBody.Jump.Jumps.Length > 0)
            _root.AddForce(Vector3.up * _stateConfig.LowerBody.Jump.Jumps[0].VerticalForce, ForceMode.Impulse);
        }
      }
    }

    private void SwitchLookAt()
    {
      if (_input.GetKeyDown(KeyCode.F))
        SwitchLookAtTargetControllerState(!IsLookAtTarget());
    }
  }
}