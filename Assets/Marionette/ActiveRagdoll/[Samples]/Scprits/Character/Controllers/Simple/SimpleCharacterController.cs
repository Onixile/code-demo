using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers._MotionExecutors_;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers._MotionExecutors_._Interfaces;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.Checker;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.StateConfigData;
using Marionette.ActiveRagdoll.Runtime._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Animation._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Joint;
using UnityEngine;

namespace Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.Simple
{
  public abstract class SimpleCharacterController : ICharacterController
  {
    protected readonly StateConfig _stateConfig;
    
    private readonly IAnimationController _animationController;
    
    private readonly PositionMotion _positionMotion;
    private readonly RotationMotion _rotationMotion;
    private readonly Grounded _groundedChecker;

    protected SimpleCharacterController(IAnimationController animationController, StateConfig stateConfig, JointDatasContainer joints,
      IMotionDirectionCalculator motionDirectionCalculator)
    {
      _animationController = animationController;
      _stateConfig = stateConfig;

      _groundedChecker = new Grounded(_stateConfig, null, joints);
      
      _positionMotion = new PositionMotion(motionDirectionCalculator, joints, CheckGrounded);
      _rotationMotion = new RotationMotion(motionDirectionCalculator, joints, CheckGrounded,
        () => _animationController.BodyTargetPointController.TargetPosition);

      SetSteeringData(_stateConfig.LowerBody.Steering.Move);
    }
    
    public virtual void Update()
    {
      _groundedChecker.Update();
      _positionMotion.Update();
      _rotationMotion.Update();
      
      SetLookAtDirection();
      RecalculateInertiaTensor();
    }

    public void LateUpdate()
    {
      _groundedChecker.LateUpdate();
      _positionMotion.LateUpdate();
      _rotationMotion.LateUpdate();
      
      SwtichMuscles();
    }

    public void FixedUpdate()
    {
      _groundedChecker.FixedUpdate();
      _positionMotion.FixedUpdate();
      _rotationMotion.FixedUpdate();
    }

    public void OnDrawGizmos()
    {
      _groundedChecker.OnDrawGizmos();
      _positionMotion.OnDrawGizmos();
      _rotationMotion.OnDrawGizmos();
    }

    protected void SetSteeringData(SteeringData.Params steeringData)
    {
      _positionMotion.SetSteeringData(steeringData);
      _rotationMotion.SetSteeringData(steeringData);
    }

    protected void SleepPositionMotion(bool value) =>
      _positionMotion.Sleep(value);
    
    protected void SleepRotationMotion(bool value) =>
      _rotationMotion.Sleep(value);
    
    protected bool CheckGrounded() =>
      _groundedChecker.Result;
    
    protected bool CheckAnimation(string animName, int animationLayer) =>
      _animationController.Animator.GetCurrentAnimatorStateInfo(animationLayer).IsName(animName);
    
    protected void PlayAnimation(string animName, int animationLayer) =>
      _animationController.Animator.Play(animName, animationLayer);
    
    protected AnimatorStateInfo GetCurrentAnimatorStateInfo(int animationLayer) =>
      _animationController.Animator.GetCurrentAnimatorStateInfo(animationLayer);

    protected AnimatorClipInfo[] GetCurrentAnimatorClipInfo(int animationLayer) =>
      _animationController.Animator.GetCurrentAnimatorClipInfo(animationLayer);
    
    protected void SetLookAtDirection() =>
      _animationController.EyesLookAtDirectionController?.SetDirection(_rotationMotion.LookDirection);

    protected void RecalculateInertiaTensor() =>
      _animationController.RecalculateInertiaTensor(CheckGrounded());

    protected void SetMuscle(JointType jointType, float spring, float damper) =>
      _animationController.MusclesController.SetMuscle(jointType, spring, damper);
    
    protected void SetMuscleToDefault(JointType jointType) =>
      _animationController.MusclesController.SetMuscleToDefault(jointType);

    protected void ResetMuscle(JointType jointType) =>
      _animationController.MusclesController.SetMuscleToDefault(jointType);

    protected void ActivateConnectionController(ConnectionData connectionData) =>
      _animationController.ConnectionController.Activate(connectionData.External);
    
    protected void DeactivateConnectionController() =>
      _animationController.ConnectionController.Deactivate();
    
    protected void SwitchChangeSolversIterations(bool toMax) =>
      _animationController.ChangeSolversIterations(toMax);
    
    protected bool LookAtTargetIsReady() =>
      _animationController.BodyTargetPointController.TargetIsReady();
    
    protected void ActivateSwitchLookAtTargetController() =>
      _animationController.BodyTargetPointController.Activate();

    protected void DeactivateSwitchLookAtTargetController() =>
      _animationController.BodyTargetPointController.Deactivate();

    protected bool IsLookAtTarget() => 
      _rotationMotion.LookAtTarget;

    protected void SwitchLookAtTargetControllerState(bool value)
    {
      if (value)
        ActivateSwitchLookAtTargetController();
      else
        DeactivateSwitchLookAtTargetController();
      
      _rotationMotion.LookAtTarget = LookAtTargetIsReady();
    }
    
    private void SwtichMuscles()
    {
      if (CheckGrounded())
        foreach (JointType jointType in _stateConfig.LowerBody.Steering.NotGrounded.OverrideMuscleData.TargetJoints)
          ResetMuscle(jointType);
      else
        foreach (JointType jointType in _stateConfig.LowerBody.Steering.NotGrounded.OverrideMuscleData.TargetJoints)
          SetMuscle(jointType, _stateConfig.LowerBody.Steering.NotGrounded.OverrideMuscleData.Spring, _stateConfig.LowerBody.Steering.NotGrounded.OverrideMuscleData.Damper);
    }
  }
}
