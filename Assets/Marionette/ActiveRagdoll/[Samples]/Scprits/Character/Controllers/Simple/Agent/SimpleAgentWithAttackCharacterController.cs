using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers._MotionExecutors_.DirectionCalculators;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine;
using Marionette.ActiveRagdoll._Samples_._Main_Demo_.Initializer;
using Marionette.ActiveRagdoll.Runtime.Animation._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Joint;
using UnityEngine;

namespace Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.Simple.Agent
{
  public class SimpleAgentWithAttackCharacterController : SimpleCharacterController
  {
    private const float MovementSensitivity = 1f;

    private readonly Rigidbody _root;
    
    private readonly int _UBAnimationLayer;
    private readonly int _LBAnimationLayer;

    private bool _lastAttackArmIsRight;
    
    public SimpleAgentWithAttackCharacterController(IAnimationController animationController, StateConfig stateConfig, JointDatasContainer joints) 
      : base(animationController, stateConfig, joints, 
        new MotionDirectionToTargetCalculator(joints.Get(JointType.Pelvis).Transform, 
          Object.FindObjectOfType<MainDemoInitializer>()?.GetComponent<JointDatasContainer>().Get(JointType.Pelvis).Transform))
    {
      _root = joints.Get(JointType.Pelvis).Rigidbody;
      
      _UBAnimationLayer = animationController.Animator.GetLayerIndex(stateConfig.UpperBody.AnimationLayer);
      _LBAnimationLayer = animationController.Animator.GetLayerIndex(stateConfig.LowerBody.AnimationLayer);
    }
    
    public override void Update()
    {
      base.Update();

      SwitchAttackAnimation();
      SwitchMoveAnimation();

      SwitchLookAt();
    }

    private void SwitchAttackAnimation()
    {
      if (!_lastAttackArmIsRight && CheckAnimation(_stateConfig.UpperBody.Attack.IdleAnimState, _UBAnimationLayer))
      {
        PlayAnimation(_stateConfig.UpperBody.Attack.RightAttackAnimState, _UBAnimationLayer);
        _lastAttackArmIsRight = true;
      }
      else if (_lastAttackArmIsRight && CheckAnimation(_stateConfig.UpperBody.Attack.IdleAnimState, _UBAnimationLayer))
      {
        PlayAnimation(_stateConfig.UpperBody.Attack.LeftAttackAnimState, _UBAnimationLayer);
        _lastAttackArmIsRight = false;
      }

      AddTemporaryConnection();

      void AddTemporaryConnection()
      {
        if (CheckAnimation(_stateConfig.UpperBody.Attack.RightAttackAnimState, _UBAnimationLayer) | CheckAnimation(_stateConfig.UpperBody.Attack.LeftAttackAnimState, _UBAnimationLayer))
        {
          AnimatorStateInfo animationState = GetCurrentAnimatorStateInfo(_UBAnimationLayer);
          AnimatorClipInfo[] myAnimatorClip = GetCurrentAnimatorClipInfo(_UBAnimationLayer);

          if (myAnimatorClip.Length == 0)
            return;

          float time = animationState.normalizedTime / myAnimatorClip[0].clip.length;

          if (time > _stateConfig.UpperBody.Attack.ConnectionStart && time < _stateConfig.UpperBody.Attack.ConnectionFinish)
            ActivateConnectionController(_stateConfig.UpperBody.Attack);
          else
            DeactivateConnectionController();
        }
      }
    }

    private void SwitchMoveAnimation()
    {
      if (CheckAnimation(_stateConfig.LowerBody.Steering.Move.EnterAnimState, _LBAnimationLayer))
        return;

      PlayAnimation(_root.velocity.magnitude > MovementSensitivity
        ? _stateConfig.LowerBody.Steering.Move.ActionAnimState
        : _stateConfig.LowerBody.Steering.Move.IdleAnimState, _LBAnimationLayer);
    }
    
    private void SwitchLookAt()
    {
      if (!IsLookAtTarget())
        SwitchLookAtTargetControllerState(true);
    }
  }
}
