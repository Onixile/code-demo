using System;
using System.Collections.Generic;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.Base;
using Marionette.ActiveRagdoll.Runtime.Joint;
using UnityEngine;

namespace Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.Checker
{
  public class Grounded : BaseChecker
  {
    public bool Result { get; private set; } = true;

    private readonly Rigidbody _rigidbody;
    private readonly JointDatasContainer _joints;

    private Vector3 _origin;
    private Vector3 _direction;

    private RaycastHit _raycastHit;

    public Grounded(StateConfig stateConfig, Dictionary<Type, IUpdatableState> checkerStates, JointDatasContainer joints)
      : base(stateConfig, checkerStates)
    {
      _joints = joints;
      _rigidbody = joints.Get(JointType.Pelvis).Rigidbody;
    }

    public override void Update()
    {
      Check();
      CheckSelfCollision();
    }

    public override void LateUpdate()
    {
    }

    public override void FixedUpdate()
    {
    }

    public override void OnDrawGizmos()
    {
      Color gColor = Gizmos.color;
      Gizmos.color = Result ? Color.white : Color.gray;
      Gizmos.DrawRay(_origin, _direction * _stateConfig.Checker.Grounded.CheckDist);
      Gizmos.color = gColor;
    }

    private void Check()
    {
      Transform transform = _rigidbody.transform;

      _origin = transform.position;
      _direction = Vector3.down;

      Result = Physics.Raycast(_origin, _direction, out _raycastHit,
        _stateConfig.Checker.Grounded.CheckDist, _stateConfig.Checker.Grounded.Mask, QueryTriggerInteraction.Ignore);
    }

    private void CheckSelfCollision()
    {
      if (Result)
      {
        foreach (JointData jointData in _joints.GetAll())
          if (jointData.Collider && _raycastHit.collider == jointData.Collider)
          {
            Result = false;
            break;
          }
      }
    }
  }
}