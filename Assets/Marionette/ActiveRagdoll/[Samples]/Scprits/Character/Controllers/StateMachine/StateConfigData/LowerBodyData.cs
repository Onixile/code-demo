using System;
using Marionette.ActiveRagdoll.Runtime.Joint;
using UnityEngine;

namespace Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.StateConfigData
{
  [Serializable]
  public partial class LowerBodyData
  {
    public string AnimationLayer = "Lower Body";
    
    public SteeringData Steering;
    public JumpData Jump;
  }

  [Serializable]
  public partial class SteeringData
  {
    public Params Move;
    public Params Crawl;
    public NotGroundedData NotGrounded;

    [Serializable]
    public partial class NotGroundedData
    {
      public MuscleData OverrideMuscleData;

      [Serializable]
      public partial class MuscleData
      {
        [Range(0, 10000)] public float Spring = 3f;
        [Range(0, 10000)] public float Damper = 1f;
        
        public JointType[] TargetJoints;
      }
    }

    [Serializable]
    public partial class Params
    {
      [Range(-1, 1)] public int UpsideDownDirection;

      public PositionParams PositionMotion;
      public RotationParams RotationMotion;

      public string EnterAnimState;
      public string IdleAnimState;
      public string ActionAnimState;

      [Serializable]
      public partial class PositionParams
      {
        [Range(0, 50)] public float Force = 1.1f;
        [Range(0, 50)] public float Smoother = 0.3f;

        public FrictionParams Friction;
      }

      [Serializable]
      public partial class RotationParams
      {
        [Range(0, 50)] public float Force = 1.1f;
        [Range(0, 50)] public float Smoother = 1.2f;

        public FrictionParams Friction;
      }

      [Serializable]
      public partial class FrictionParams
      {
        [Range(0, 50)] public float Static = 0.5f;
        [Range(0, 50)] public float Active = 0.25f;
      }
    }
  }

  [Serializable]
  public partial class JumpData
  {
    [Range(0, 1f)] public float IgnoreGroundedCheckerDelay = 0.1f;
    [Range(0, 1)] public float BodyIsHorizontalValue = 0.5f;

    public Params[] Jumps;

    [Serializable]
    public partial class Params
    {
      [Range(0, 5)] public float ExitDelay = 0.05f;
      [Range(0, 300)] public float HorizontalForce = 10;
      [Range(0, 300)] public float VerticalForce = 45;
    }
  }
}