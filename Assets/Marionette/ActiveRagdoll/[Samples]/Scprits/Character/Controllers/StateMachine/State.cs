using System;
using System.Collections.Generic;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.Base;

namespace Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine
{
  public abstract class State
  {
    protected readonly StateConfig _stateConfig;
    private readonly Dictionary<Type, IUpdatableState> _checkerStates;

    protected State(StateConfig stateConfig, Dictionary<Type, IUpdatableState> checkerStates)
    {
      _stateConfig = stateConfig;
      _checkerStates = checkerStates;
    }

    protected TState GetCheckerState<TState>() where TState : BaseChecker =>
      _checkerStates[typeof(TState)] as TState;
  }
}