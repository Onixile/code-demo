using System;
using System.Collections.Generic;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers._MotionExecutors_;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.Base;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.StateConfigData;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.StateMachines;
using Marionette.ActiveRagdoll.Runtime._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Animation._Interfaces;
using UnityEngine;

namespace Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.Behaviour.LB.Steering
{
  public class Move : BaseSteer
  {
    public Move(StateConfig stateConfig, SteeringData.Params data, IInput input, IAnimationController animationController,
      BehaviourStateMachine behaviourStateMachine, PositionMotion positionMotion, RotationMotion rotationMotion, Dictionary<Type, IUpdatableState> checkerStates)
      : base(stateConfig, input, animationController, behaviourStateMachine, positionMotion, rotationMotion, data, checkerStates)
    {
    }

    public override void Enter()
    {
      base.Enter();
      PlayAnimation(_stateConfig.LowerBody.Steering.Move.EnterAnimState);
    }

    public override void Update()
    {
      base.Update();

      if (_input.GetKeyDown(KeyCode.Space))
      {
        if (CheckGrounded())
          GoToState<Jump>();
      }
    }

    protected override void SwitchAnimation()
    {
      if (CheckAnimation(_stateConfig.LowerBody.Steering.Move.EnterAnimState))
        return;

      PlayAnimation(!_input.AxisIsZero() 
        ? _stateConfig.LowerBody.Steering.Move.ActionAnimState 
        : _stateConfig.LowerBody.Steering.Move.IdleAnimState);
    }
  }
}