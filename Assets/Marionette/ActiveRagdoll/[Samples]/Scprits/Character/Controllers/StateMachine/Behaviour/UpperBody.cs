using System;
using System.Collections.Generic;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.Base;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.Behaviour.UB;
using Marionette.ActiveRagdoll.Runtime._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Animation._Interfaces;
using UnityEngine;

namespace Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.Behaviour
{
  public abstract class UpperBody : BaseBehaviour
  {
    protected UpperBody(StateConfig stateConfig, IInput input, IAnimationController animationController,
      StateMachines.StateMachine behaviourStateMachine, Dictionary<Type, IUpdatableState> checkerStates)
      : base(stateConfig, input, animationController, stateConfig.UpperBody.AnimationLayer, behaviourStateMachine, checkerStates)
    {
    }

    public override void Enter()
    {
      base.Enter();
      DeactivateConnectionController();
    }

    protected void TryGoToStateCrawl()
    {
      if (_input.GetKey(KeyCode.Q) && CheckGrounded() && !CheckState<Crawl>())
        GoToState<Crawl>();
    }
  }
}