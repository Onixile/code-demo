using System;
using System.Collections.Generic;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.Base;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.Checker;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.StateMachines;
using Marionette.ActiveRagdoll.Runtime.Animation._Interfaces;
using UnityEngine;

namespace Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.Condition
{
  public class Knockout : BaseCondition
  {
    private readonly Grounded _groundedChecker;
    private float _timer;
 
    public Knockout(StateConfig stateConfig, IAnimationController animationController,
      BehaviourStateMachine upperBodyStates, BehaviourStateMachine lowerBodyStates, ConditionStateMachine conditionStateMachine,
       Dictionary<Type, IUpdatableState> checkerStates) 
      : base(stateConfig, animationController, upperBodyStates, lowerBodyStates, conditionStateMachine, checkerStates) =>
      _groundedChecker = GetCheckerState<Grounded>();

    public override void Enter()
    {
      base.Enter();

      _behaviourIsActive = false;
      _timer = 0;
      SetActiveMuscles(_stateConfig.Condition.Knockout.MusclesIsActive);

      EnterToIdleBehaviourState();
    }
    
    protected override void CheckCondition()
    {
      if(_groundedChecker.Result)
        _timer += Time.deltaTime;

      if (_timer >= _stateConfig.Condition.Knockout.Duration)
        GoToState<Active>();
    }
  }
}