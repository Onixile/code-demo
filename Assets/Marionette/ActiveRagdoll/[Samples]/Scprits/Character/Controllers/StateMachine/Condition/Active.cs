using System;
using System.Collections.Generic;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.Base;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.StateMachines;
using Marionette.ActiveRagdoll.Runtime._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Animation._Interfaces;
using UnityEngine;

namespace Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.Condition
{
  public class Active : BaseCondition
  {
    private readonly IInput _input;

    public Active(StateConfig stateConfig, IInput input, IAnimationController animationController,
      BehaviourStateMachine upperBodyStates, BehaviourStateMachine lowerBodyStates, ConditionStateMachine conditionStateMachine,
      Dictionary<Type, IUpdatableState> checkerStates)
      : base(stateConfig, animationController, upperBodyStates, lowerBodyStates, conditionStateMachine, checkerStates) =>
      _input = input;

    public override void Enter()
    {
      base.Enter();

      SetActiveMuscles(_stateConfig.Condition.Active.MusclesIsActive);
      EnterToIdleBehaviourState();
    }

    protected override void CheckCondition()
    {
      if (_input.GetKeyDown(KeyCode.N))
        GoToState<Knockout>();
    }
  }
}