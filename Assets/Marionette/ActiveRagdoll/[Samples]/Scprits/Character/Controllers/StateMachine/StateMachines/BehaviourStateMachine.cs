namespace Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.StateMachines
{
  public class BehaviourStateMachine : StateMachine
  {
    public override void Update() => 
      ((IState)_activeState).Update();

    public override void LateUpdate() => 
      ((IState)_activeState).LateUpdate();
    
    public override void FixedUpdate() =>
      ((IState)_activeState).FixedUpdate();

    public override void OnDrawGizmos() =>
      ((IState)_activeState).OnDrawGizmos();
  }
}