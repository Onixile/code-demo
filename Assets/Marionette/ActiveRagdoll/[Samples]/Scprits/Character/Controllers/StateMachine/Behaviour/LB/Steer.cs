using System;
using System.Collections.Generic;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers._MotionExecutors_;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers._MotionExecutors_._Interfaces;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.Behaviour.LB.Steering;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.StateMachines;
using Marionette.ActiveRagdoll.Runtime._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Animation._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Joint;
using UnityEngine;
using Crawl = Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.Behaviour.LB.Steering.Crawl;

namespace Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.Behaviour.LB
{
  public class Steer : LowerBody
  {
    private readonly BehaviourStateMachine _steerStates;

    private readonly PositionMotion _positionMotion;
    private readonly RotationMotion _rotationMotion;

    public Steer(StateConfig stateConfig, IMotionDirectionCalculator motionDirectionCalculator, IInput input, IAnimationController animationController,
      BehaviourStateMachine behaviourStateMachine, JointDatasContainer jointDatasContainer, Dictionary<Type, IUpdatableState> checkerStates)
      : base(stateConfig, input, animationController, behaviourStateMachine, checkerStates)
    {
      _positionMotion = new PositionMotion(motionDirectionCalculator, jointDatasContainer, CheckGrounded);
      _rotationMotion = new RotationMotion(motionDirectionCalculator, jointDatasContainer, CheckGrounded, 
        () => animationController.BodyTargetPointController.TargetPosition);

      _steerStates = new BehaviourStateMachine();
      _steerStates.SetStates<Move>(new Dictionary<Type, IExitableState>
      {
        [typeof(Move)] = new Move(stateConfig, stateConfig.LowerBody.Steering.Move, input, animationController, behaviourStateMachine,
          _positionMotion, _rotationMotion, checkerStates),
        [typeof(Crawl)] = new Crawl(stateConfig, stateConfig.LowerBody.Steering.Crawl, input, animationController, behaviourStateMachine,
          _positionMotion, _rotationMotion, checkerStates),
      });
    }

    public override void Enter()
    {
      base.Enter();

      _positionMotion.ResetDirections();
      _rotationMotion.ResetDirections();

      GoToState<Move>(_steerStates);
    }

    public override void Update()
    {
      base.Update();

      if (_input.GetKey(KeyCode.Q) && CheckGrounded() && !_steerStates.CurrentStateIs(typeof(Crawl)))
        GoToState<Crawl>(_steerStates);
      else if (_input.GetKeyUp(KeyCode.Q))
        GoToState<Move>(_steerStates);

      _steerStates.Update();
      
      SetLookAtDirection(_rotationMotion.LookDirection);
    }

    public override void LateUpdate()
    {
      base.LateUpdate();
      _steerStates.LateUpdate();
    }

    public override void FixedUpdate()
    {
      base.FixedUpdate();
      _steerStates.FixedUpdate();
    }

    public override void OnDrawGizmos()
    {
      base.OnDrawGizmos();
      _steerStates.OnDrawGizmos();
    }
  }
}