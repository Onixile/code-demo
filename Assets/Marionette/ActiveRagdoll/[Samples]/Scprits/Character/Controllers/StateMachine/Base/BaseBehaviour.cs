using System;
using System.Collections.Generic;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.Checker;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.StateConfigData;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.StateMachines;
using Marionette.ActiveRagdoll.Runtime._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Animation._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Animation.Connection;
using Marionette.ActiveRagdoll.Runtime.Joint;
using UnityEngine;
using ConnectionData = Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.StateConfigData.ConnectionData;

namespace Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.Base
{
  public abstract class BaseBehaviour : State, IBehaviourState
  {
    protected readonly IInput _input;

    private readonly int _bodyAnimationLayer;
    private readonly IAnimationController _animationController;
    private readonly StateMachines.StateMachine _behaviourStateMachine;
    private readonly Grounded _groundedChecker;

    protected BaseBehaviour(StateConfig stateConfig, IInput input, IAnimationController animationController, string bodyAnimationLayer,
      StateMachines.StateMachine behaviourStateMachine, Dictionary<Type, IUpdatableState> checkerStates)
      : base(stateConfig, checkerStates)
    {
      _input = input;
      _animationController = animationController;
      _bodyAnimationLayer = animationController.Animator.GetLayerIndex(bodyAnimationLayer);
      _behaviourStateMachine = behaviourStateMachine;
      _groundedChecker = GetCheckerState<Grounded>();
    }

    public virtual void Enter()
    {
    }

    public virtual void Exit()
    {
    }

    public virtual void Update()
    {
    }

    public virtual void LateUpdate()
    {
    }

    public virtual void FixedUpdate()
    {
    }

    public virtual void OnDrawGizmos()
    {
    }

    protected bool CheckState<TState>() where TState : class, IState =>
      _behaviourStateMachine.CurrentStateIs(typeof(TState));

    protected bool CheckState<TState>(BehaviourStateMachine stateMachine) where TState : class, IState =>
      stateMachine.CurrentStateIs(typeof(TState));

    protected void GoToState<TState>() where TState : class, IState =>
      _behaviourStateMachine.Enter<TState>();

    protected void GoToState<TState>(BehaviourStateMachine stateMachine) where TState : class, IState =>
      stateMachine.Enter<TState>();

    protected bool CheckGrounded() =>
      _groundedChecker.Result;

    protected bool CheckAnimation(string animName) =>
      _animationController.Animator.GetCurrentAnimatorStateInfo(_bodyAnimationLayer).IsName(animName);

    protected void PlayAnimation(string animName) =>
      _animationController.Animator.Play(animName, _bodyAnimationLayer);

    protected void PlayAnimation(string animName, int animationLayer) =>
      _animationController.Animator.Play(animName, animationLayer);

    protected AnimatorStateInfo GetCurrentAnimatorStateInfo() =>
      _animationController.Animator.GetCurrentAnimatorStateInfo(_bodyAnimationLayer);

    protected AnimatorClipInfo[] GetCurrentAnimatorClipInfo() =>
      _animationController.Animator.GetCurrentAnimatorClipInfo(_bodyAnimationLayer);

    protected void SetAnimationLayerWeight(int animLayer, float animWeight) =>
      _animationController.Animator.SetLayerWeight(animLayer, animWeight);

    protected void ActivateSwitchLookAtTargetController() =>
      _animationController.BodyTargetPointController.Activate();

    protected void DeactivateSwitchLookAtTargetController() =>
      _animationController.BodyTargetPointController.Deactivate();

    protected bool ConnectionControllerIsConnected() =>
      _animationController.ConnectionController.IsConnected();

    protected bool ConnectionControllerIsConnected(ConnectionType connectionType) =>
      _animationController.ConnectionController.IsConnected(connectionType);

    protected Rigidbody ConnectionControllerGetConnection(ConnectionType connectionType) => 
      _animationController.ConnectionController.GetConnection(connectionType);

    protected void ActivateConnectionController(ConnectionData connectionData) =>
      _animationController.ConnectionController.Activate(connectionData.External);

    protected void ActivateConnectionController(ConnectionData connectionData, ConnectionType connectionType) =>
      _animationController.ConnectionController.Activate(connectionData.External, connectionType);

    protected void DeactivateConnectionController() =>
      _animationController.ConnectionController.Deactivate();

    protected void DeactivateConnectionController(ConnectionType connectionType) =>
      _animationController.ConnectionController.Deactivate(connectionType);

    protected void SwitchChangeSolversIterations(bool toMax) =>
      _animationController.ChangeSolversIterations(toMax);

    protected void SetMuscles(JointType jointType, float spring, float damper) =>
      _animationController.MusclesController.SetMuscle(jointType, spring, damper);

    protected void ResetMuscles(JointType jointType) =>
      _animationController.MusclesController.SetMuscleToDefault(jointType);

    protected void SetLookAtDirection(Vector3 direction) =>
      _animationController.EyesLookAtDirectionController?.SetDirection(direction);

    protected bool LookAtTargetIsReady() =>
      _animationController.BodyTargetPointController.TargetIsReady();
  }
}