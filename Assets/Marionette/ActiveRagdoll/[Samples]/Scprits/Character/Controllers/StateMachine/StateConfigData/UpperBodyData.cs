using System;
using Marionette.ActiveRagdoll.Runtime.Animation.Connection;
using UnityEngine;

namespace Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.StateConfigData
{
  [Serializable]
  public partial class UpperBodyData
  {
    public string AnimationLayer = "Upper Body";
    public string AnimationLayerClimbingR = "Climbing-UB-R";
    public string AnimationLayerClimbingL = "Climbing-UB-L";

    public IdleData Idle;
    public AttackData Attack;
    public GrabData Grab;
    public CrawlData Crawl;
  }

  [Serializable]
  public partial class ConnectionData
  {
    public ConnectionExternalData External;
  }

  [Serializable]
  public partial class IdleData
  {
    public string IdleAnimState;
  }

  [Serializable]
  public partial class AttackData : ConnectionData
  {
    [Range(0, 1)] public float ConnectionStart = 0.6f;
    [Range(0, 1)] public float ConnectionFinish = 0.8f;

    public string IdleAnimState;
    public string RightAttackAnimState;
    public string LeftAttackAnimState;
  }

  [Serializable]
  public partial class GrabData : ConnectionData
  {
    [Range(0, 50)] public float ClimbingRotationForce = 1.5f;
    [Range(0, 1)] public float ToClimbingTransitionSpeed = 15;
    [Range(0, 2)] public float ClimbingUpDelay = 0.5f;

    public string IdleAnimState;
    public string GrabAnimState;
    public string ClimbAnimState;
  }

  [Serializable]
  public partial class CrawlData
  {
    public string IdleAnimState;
    public string CrawlAnimState;
  }
}