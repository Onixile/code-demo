using System;
using System.Collections.Generic;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers._MotionExecutors_;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.Behaviour;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.StateConfigData;
using Marionette.ActiveRagdoll.Runtime._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Animation._Interfaces;
using UnityEngine;

namespace Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.Base
{
  public abstract class BaseSteer : LowerBody
  {
    protected readonly SteeringData.Params _data;

    private readonly PositionMotion _positionMotion;
    private readonly RotationMotion _rotationMotion;

    protected BaseSteer(StateConfig stateConfig, IInput input, IAnimationController animationController,
      StateMachines.StateMachine behaviourStateMachine, PositionMotion positionMotion, RotationMotion rotationMotion,
      SteeringData.Params data, Dictionary<Type, IUpdatableState> checkingStates)
      : base(stateConfig, input, animationController, behaviourStateMachine, checkingStates)
    {
      _data = data;

      _positionMotion = positionMotion;
      _rotationMotion = rotationMotion;
    }

    public override void Enter()
    {
      base.Enter();

      _positionMotion.SetSteeringData(_data);
      _rotationMotion.SetSteeringData(_data);
    }

    public override void Update()
    {
      SwitchLookAtTargetControllerState();

      _positionMotion.Update();
      _rotationMotion.Update();

      SwitchAnimation();
    }

    public override void LateUpdate()
    {
      _positionMotion.LateUpdate();
      _rotationMotion.LateUpdate();
    }

    public override void FixedUpdate()
    {
      base.FixedUpdate();

      _positionMotion.FixedUpdate();
      _rotationMotion.FixedUpdate();
    }

    public override void OnDrawGizmos()
    {
      base.OnDrawGizmos();

      _positionMotion.OnDrawGizmos();
      _rotationMotion.OnDrawGizmos();
    }

    protected abstract void SwitchAnimation();

    private void SwitchLookAtTargetControllerState()
    {
      if (_input.GetKeyDown(KeyCode.F))
      {
        if (!_rotationMotion.LookAtTarget)
          ActivateSwitchLookAtTargetController();
        else
          DeactivateSwitchLookAtTargetController();
      }
      
      _rotationMotion.LookAtTarget = LookAtTargetIsReady();
    }
  }
}