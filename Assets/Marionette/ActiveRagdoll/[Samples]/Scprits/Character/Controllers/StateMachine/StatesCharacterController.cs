using System;
using System.Collections.Generic;
using System.Linq;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.StateMachines;
using Marionette.ActiveRagdoll.Runtime._Interfaces;

namespace Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine 
{
  public class StatesCharacterController : ICharacterController
  {
    protected readonly Dictionary<Type, IUpdatableState> _checkerStates;
    protected readonly BehaviourStateMachine _upperBodyBehaviourStates;
    protected readonly BehaviourStateMachine _lowerBodyBehaviourStates;
    protected readonly ConditionStateMachine _conditionStates;

    public StatesCharacterController(Dictionary<Type, IUpdatableState> checkerStates,
      BehaviourStateMachine upperBodyBehaviourStates,
      BehaviourStateMachine lowerBodyBehaviourStates,
      ConditionStateMachine conditionStates)
    {
      _checkerStates = checkerStates;
      _upperBodyBehaviourStates = upperBodyBehaviourStates;
      _lowerBodyBehaviourStates = lowerBodyBehaviourStates;
      _conditionStates = conditionStates;
    }

    public virtual void Update()
    {
      _checkerStates.Values.ToList().ForEach(s => s.Update());
      _conditionStates.Update();
    }

    public virtual void LateUpdate()
    {
      _checkerStates.Values.ToList().ForEach(s => s.LateUpdate());
      _conditionStates.LateUpdate();
    }

    public virtual void FixedUpdate()
    {
      _checkerStates.Values.ToList().ForEach(s => s.FixedUpdate());
      _conditionStates.FixedUpdate();
    }

    public virtual void OnDrawGizmos()
    {
      _checkerStates.Values.ToList().ForEach(s => s.OnDrawGizmos());
      _conditionStates.OnDrawGizmos();
    }
  }
}