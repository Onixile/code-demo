using Marionette.ActiveRagdoll.Runtime._Interfaces;

namespace Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine
{
  public interface IExitableState
  {
    void Exit();
  }

  public interface IEnterableState
  {
    void Enter();
  }
  
  public interface IUpdatableState : IUpdate, IDrawGizmos
  {

  }
  
  public interface IPayloadedState<TPayload> : IExitableState
  {
    void Enter(TPayload payload);
  }
  
  public interface IState : IUpdatableState, IEnterableState, IExitableState
  {

  }

  public interface IBehaviourState : IState
  {
  }

  public interface IConditionState : IState
  {
    void BackgroundUpdate();
  }
}