using System;
using System.Collections.Generic;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers._MotionExecutors_;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.Base;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.StateConfigData;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.StateMachines;
using Marionette.ActiveRagdoll.Runtime._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Animation._Interfaces;

namespace Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.Behaviour.LB.Steering
{
  public class Crawl : BaseSteer
  {
    public Crawl(StateConfig stateConfig, SteeringData.Params data, IInput input, IAnimationController animationController,
      BehaviourStateMachine behaviourStateMachine, PositionMotion positionMotion, RotationMotion rotationMotion, Dictionary<Type, IUpdatableState> checkerStates)
      : base(stateConfig, input, animationController, behaviourStateMachine, positionMotion, rotationMotion, data, checkerStates)
    {
    }

    public override void Enter()
    {
      base.Enter();
      PlayAnimation(_stateConfig.LowerBody.Steering.Crawl.EnterAnimState);
    }

    protected override void SwitchAnimation()
    {
      if (!CheckAnimation(_stateConfig.LowerBody.Steering.Crawl.IdleAnimState) && !CheckAnimation(_stateConfig.LowerBody.Steering.Crawl.ActionAnimState))
        return;

      PlayAnimation(!_input.AxisIsZero()
        ? _stateConfig.LowerBody.Steering.Crawl.ActionAnimState
        : _stateConfig.LowerBody.Steering.Crawl.IdleAnimState);
    }
  }
}