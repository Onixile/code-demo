using System;
using System.Collections.Generic;
using Marionette.ActiveRagdoll.Runtime._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Animation._Interfaces;
using UnityEngine;

namespace Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.Behaviour.UB
{
  public class Attack : UpperBody
  {
    private bool _lastArmIsRight;

    public Attack(StateConfig stateConfig, IInput input, IAnimationController animationController,
      StateMachines.StateMachine behaviourStateMachine, Dictionary<Type, IUpdatableState> checkerStates)
      : base(stateConfig, input, animationController, behaviourStateMachine, checkerStates) =>
      _lastArmIsRight = false;

    public override void Enter()
    {
      base.Enter();
      SwitchAnimation();
    }

    public override void Update()
    {
      base.Update();

      if (CheckAnimation(_stateConfig.UpperBody.Attack.IdleAnimState))
        GoToState<Idle>();

      AddTemporaryConnection();
    }

    private void SwitchAnimation()
    {
      if (!_lastArmIsRight && CheckAnimation(_stateConfig.UpperBody.Attack.IdleAnimState) && _input.GetKey(KeyCode.Mouse0))
      {
        PlayAnimation(_stateConfig.UpperBody.Attack.RightAttackAnimState);
        _lastArmIsRight = true;
      }
      else if (_lastArmIsRight && CheckAnimation(_stateConfig.UpperBody.Attack.IdleAnimState) && _input.GetKey(KeyCode.Mouse0))
      {
        PlayAnimation(_stateConfig.UpperBody.Attack.LeftAttackAnimState);
        _lastArmIsRight = false;
      }
    }

    private void AddTemporaryConnection()
    {
      if (CheckAnimation(_stateConfig.UpperBody.Attack.RightAttackAnimState) || CheckAnimation(_stateConfig.UpperBody.Attack.LeftAttackAnimState))
      {
        AnimatorStateInfo animationState = GetCurrentAnimatorStateInfo();
        AnimatorClipInfo[] myAnimatorClip = GetCurrentAnimatorClipInfo();

        if (myAnimatorClip.Length == 0)
          return;

        float time = animationState.normalizedTime / myAnimatorClip[0].clip.length;

        if (time > _stateConfig.UpperBody.Attack.ConnectionStart && time < _stateConfig.UpperBody.Attack.ConnectionFinish)
          ActivateConnectionController(_stateConfig.UpperBody.Attack);
        else
          DeactivateConnectionController();
      }
    }
  }
}