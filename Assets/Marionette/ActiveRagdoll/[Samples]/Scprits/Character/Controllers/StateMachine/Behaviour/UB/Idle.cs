using System;
using System.Collections.Generic;
using Marionette.ActiveRagdoll.Runtime._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Animation._Interfaces;
using UnityEngine;

namespace Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.Behaviour.UB
{
  public class Idle : UpperBody
  {
    public Idle(StateConfig stateConfig, IInput input, IAnimationController animationController,
      StateMachines.StateMachine behaviourStateMachine, Dictionary<Type, IUpdatableState> checkerStates)
      : base(stateConfig, input, animationController, behaviourStateMachine, checkerStates)
    {
    }

    public override void Enter()
    {
      base.Enter();
      SwitchAnimation();
    }

    public override void Update()
    {
      base.Update();

      if (CheckAnimation(_stateConfig.UpperBody.Idle.IdleAnimState))
      {
        if (_input.GetKey(KeyCode.Mouse0))
          GoToState<Attack>();
        else if (_input.GetKey(KeyCode.E))
          GoToState<Grab>();
      }

      TryGoToStateCrawl();
    }

    private void SwitchAnimation()
    {
      if (!CheckAnimation(_stateConfig.UpperBody.Idle.IdleAnimState))
        PlayAnimation(_stateConfig.UpperBody.Idle.IdleAnimState);
    }
  }
}