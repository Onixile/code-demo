using System.Linq;

namespace Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.StateMachines
{
  public class ConditionStateMachine : BehaviourStateMachine
  {
    public override void Update()
    {
      base.Update();
      BackgroundUpdate();
    }

    private void BackgroundUpdate() => 
      _states.Values.Cast<IConditionState>().ToList().ForEach(s => s.BackgroundUpdate());
  }
}