using System;
using UnityEngine;

namespace Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.StateConfigData
{
  [Serializable]
  public partial class CheckerData
  {
    public GroundedData Grounded;
  }
  
  [Serializable]
  public partial class GroundedData
  {
    public LayerMask Mask;
    [Range(0, 100)] public float CheckDist = 0.75f;
  }
}
