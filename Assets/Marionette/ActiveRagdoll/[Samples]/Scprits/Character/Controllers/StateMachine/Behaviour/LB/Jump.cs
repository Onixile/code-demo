using System;
using System.Collections.Generic;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.StateConfigData;
using Marionette.ActiveRagdoll.Runtime._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Animation._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Joint;
using UnityEngine;

namespace Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.Behaviour.LB
{
  public class Jump : LowerBody
  {
    private readonly Rigidbody _root;
    private JumpData.Params _currentParams;

    private float _exitDelay;
    private float _ignoreGroundedCheckerTimer;
    private int _amountOfJumps;

    public Jump(StateConfig stateConfig, IInput input, IAnimationController animationController,
      StateMachines.StateMachine behaviourStateMachine, JointDatasContainer jointDatasContainer, Dictionary<Type, IUpdatableState> checkerStates)
      : base(stateConfig, input, animationController, behaviourStateMachine, checkerStates) =>
      _root = jointDatasContainer.Get(JointType.Pelvis).Rigidbody;

    public override void Enter()
    {
      base.Enter();

      _amountOfJumps = 0;
      _ignoreGroundedCheckerTimer = 0;

      DoJump();
    }

    public override void Update()
    {
      base.Update();

      UpdateCheckerTimer(out bool ignoreGroundedChecker);
      CalculateExitDelay(ignoreGroundedChecker);

      if (ignoreGroundedChecker == false && CheckGrounded() && _exitDelay == 0)
        GoToState<Steer>();
      else
      {
        if (_input.GetKeyDown(KeyCode.Space))
          DoAdditionalJump();
      }
    }

    public override void FixedUpdate()
    {
      base.FixedUpdate();

      if (_currentParams == null)
        return;

      if (CheckGrounded())
        SlowdownRotationForce();
    }

    private void UpdateCheckerTimer(out bool ignoreGroundedChecker)
    {
      if (CheckGrounded())
        _ignoreGroundedCheckerTimer += Time.deltaTime;
      else
      {
        float ratio = 1 - Vector3.Angle(Vector3.down, -_root.transform.up) / 180;

        if (ratio < _stateConfig.LowerBody.Jump.BodyIsHorizontalValue)
          ResetExitDelay();
      }

      ignoreGroundedChecker = _ignoreGroundedCheckerTimer < _stateConfig.LowerBody.Jump.IgnoreGroundedCheckerDelay;
    }

    private void CalculateExitDelay(bool ignoreGroundedChecker)
    {
      if (_exitDelay > 0)
      {
        if (!ignoreGroundedChecker && CheckGrounded())
        {
          _exitDelay -= Time.deltaTime;
          _exitDelay = Mathf.Clamp(_exitDelay, 0, _exitDelay);
        }
      }
    }

    private void ResetExitDelay() =>
      _exitDelay = _currentParams?.ExitDelay ?? _stateConfig.LowerBody.Jump.IgnoreGroundedCheckerDelay;

    private void DoJump()
    {
      SetParams();

      AddVerticalForce();
      AddHorizontalForce();
    }

    private void DoAdditionalJump()
    {
      if (_amountOfJumps < _stateConfig.LowerBody.Jump.Jumps.Length - 1)
      {
        _amountOfJumps++;
        DoJump();
      }
    }

    private void SetParams()
    {
      int value = Mathf.Clamp(_amountOfJumps, 0, _stateConfig.LowerBody.Jump.Jumps.Length - 1);

      if (_stateConfig.LowerBody.Jump.Jumps.Length > 0)
        _currentParams = _stateConfig.LowerBody.Jump.Jumps[value];

      ResetExitDelay();
    }

    private void AddVerticalForce()
    {
      if (_currentParams == null)
        return;

      _root.AddForce(Vector3.up * _currentParams.VerticalForce, ForceMode.Impulse);
    }

    private void AddHorizontalForce()
    {
      Vector3 velocity = _root.velocity;
      velocity.y = 0;

      Vector3 horizontalDirection = Vector3.ClampMagnitude(velocity, 1);

      if (_currentParams?.HorizontalForce != null)
      {
        Vector3 force = horizontalDirection * _currentParams.HorizontalForce - velocity;
        _root.AddForce(force, ForceMode.Impulse);
      }
    }

    private void SlowdownRotationForce() =>
      _root.AddTorque(_root.angularVelocity * -1, ForceMode.VelocityChange);
  }
}