using System;
using System.Collections.Generic;

namespace Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.Base
{
  public abstract class BaseChecker : State, IUpdatableState
  {
    protected BaseChecker(StateConfig stateConfig, Dictionary<Type, IUpdatableState> checkerStates) : base(stateConfig, checkerStates)
    {
    }

    public abstract void Update();
    
    public abstract void LateUpdate();

    public abstract void FixedUpdate();

    public abstract void OnDrawGizmos();
  }
}