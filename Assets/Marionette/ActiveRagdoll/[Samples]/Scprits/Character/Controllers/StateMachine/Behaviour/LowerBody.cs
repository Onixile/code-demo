using System;
using System.Collections.Generic;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.Base;
using Marionette.ActiveRagdoll.Runtime._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Animation._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Joint;

namespace Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.Behaviour
{
  public abstract class LowerBody : BaseBehaviour
  {
    protected LowerBody(StateConfig stateConfig, IInput input, IAnimationController animationController,
      StateMachines.StateMachine behaviourStateMachine, Dictionary<Type, IUpdatableState> checkerStates)
      : base(stateConfig, input, animationController, stateConfig.LowerBody.AnimationLayer, behaviourStateMachine, checkerStates)
    {
    }

    public override void LateUpdate()
    {
      base.LateUpdate();
      SwtichMuscles();
    }

    private void SwtichMuscles()
    {
      if (CheckGrounded())
        foreach (JointType jointType in _stateConfig.LowerBody.Steering.NotGrounded.OverrideMuscleData.TargetJoints)
          ResetMuscles(jointType);
      else
        foreach (JointType jointType in _stateConfig.LowerBody.Steering.NotGrounded.OverrideMuscleData.TargetJoints)
          SetMuscles(jointType, _stateConfig.LowerBody.Steering.NotGrounded.OverrideMuscleData.Spring, _stateConfig.LowerBody.Steering.NotGrounded.OverrideMuscleData.Damper);
    }
  }
}