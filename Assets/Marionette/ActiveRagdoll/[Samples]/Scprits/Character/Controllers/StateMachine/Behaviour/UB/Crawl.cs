using System;
using System.Collections.Generic;
using Marionette.ActiveRagdoll.Runtime._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Animation._Interfaces;
using UnityEngine;

namespace Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.Behaviour.UB
{
  public class Crawl : UpperBody
  {
    public Crawl(StateConfig stateConfig, IInput input, IAnimationController animationController,
      StateMachines.StateMachine behaviourStateMachine, Dictionary<Type, IUpdatableState> checkerStates)
      : base(stateConfig, input, animationController, behaviourStateMachine, checkerStates)
    {
    }

    public override void Enter()
    {
      base.Enter();
      SwitchAnimation();
    }

    public override void Update()
    {
      base.Update();

      if (_input.GetKeyUp(KeyCode.Q))
        GoToState<Idle>();
      else if (_input.GetKey(KeyCode.Q)) 
        SwitchAnimation();
    }

    private void SwitchAnimation()
    {
      if (!_input.AxisIsZero())
      {
        if (!CheckAnimation(_stateConfig.UpperBody.Crawl.CrawlAnimState))
          PlayAnimation(_stateConfig.UpperBody.Crawl.CrawlAnimState);
      }
      else
        PlayAnimation(_stateConfig.UpperBody.Crawl.IdleAnimState);
    }
  }
}