using System;
using System.Collections.Generic;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers._MotionExecutors_._Interfaces;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers._MotionExecutors_.DirectionCalculators;
using Marionette.ActiveRagdoll.Runtime._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Animation._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Animation.Connection;
using Marionette.ActiveRagdoll.Runtime.Joint;
using UnityEngine;

namespace Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.Behaviour.UB
{
  public class Grab : UpperBody
  {
    private readonly Rigidbody _root;
    private readonly IMotionDirectionCalculator _motionDirectionCalculator;
    private Vector3 _motionDirection;

    private readonly int _animationLayerClimbingR;
    private readonly int _animationLayerClimbingL;
    private float _climbingLayerWeightR;
    private float _climbingLayerWeightL;
    private float _climbingUpDelay;
    private bool _climbingLastGroundedUp;

    public Grab(StateConfig stateConfig, IInput input, ICamera camera, IAnimationController animationController,
      StateMachines.StateMachine behaviourStateMachine, Dictionary<Type, IUpdatableState> checkerStates, JointDatasContainer jointDatasContainer)
      : base(stateConfig, input, animationController, behaviourStateMachine, checkerStates)
    {
      _root = jointDatasContainer.Get(JointType.Pelvis).Rigidbody;
      _motionDirectionCalculator = new InputMotionDirectionCalculator(_input, camera);

      _animationLayerClimbingR = animationController.Animator.GetLayerIndex(stateConfig.UpperBody.AnimationLayerClimbingR);
      _animationLayerClimbingL = animationController.Animator.GetLayerIndex(stateConfig.UpperBody.AnimationLayerClimbingL);

      ResetDirections();
    }

    public override void Enter()
    {
      base.Enter();
      SwitchAnimation();
      ResetDirections();
      ActivateConnectionController(_stateConfig.UpperBody.Grab);
      SwitchChangeSolversIterations(true);
    }

    public override void Exit()
    {
      base.Exit();
      SwitchChangeSolversIterations(false);
      ClimbingResetAnimation();
    }

    public override void Update()
    {
      base.Update();

      if (_input.GetKeyUp(KeyCode.E))
        GoToState<Idle>();

      TryGoToStateCrawl();

      SetDirection();

      ClimbingDelay();
      ClimbingSwitchAnimation();
      ClimbingSwitchConnection();
    }

    public override void FixedUpdate()
    {
      base.FixedUpdate();
      ClimbingRotate();
    }

    private void SwitchAnimation()
    {
      if (CheckAnimation(_stateConfig.UpperBody.Grab.IdleAnimState) && _input.GetKeyDown(KeyCode.E))
        PlayAnimation(_stateConfig.UpperBody.Grab.GrabAnimState);
    }

    private void ResetDirections() =>
      _motionDirection = Vector3.Cross(_root.transform.right, Vector3.up);

    private void SetDirection()
    {
      _motionDirection = _motionDirectionCalculator.Get() * -1;
      _motionDirection.y = 0;
    }

    private void ClimbingDelay()
    {
      if (_input.GetKey(KeyCode.Space) && CheckGrounded())
      {
        _climbingUpDelay += Time.deltaTime;
        _climbingLastGroundedUp = true;
      }
      else if (_input.GetKeyUp(KeyCode.Space) && !CheckGrounded())
      {
        if (!_climbingLastGroundedUp)
          _climbingUpDelay = _stateConfig.UpperBody.Grab.ClimbingUpDelay;

        _climbingLastGroundedUp = false;
      }
      else if (_input.GetKeyUp(KeyCode.Space) || CheckGrounded())
        _climbingUpDelay = 0;
    }

    private void ClimbingRotate()
    {
      if (!CheckGrounded() && ConnectionControllerIsConnected())
      {
        if (!_input.AxisIsZero() && ClimbingCheckInput())
        {
          Quaternion lookRotation = Quaternion.LookRotation(_motionDirection);

          Quaternion targetRotation = GetShortestRotation(lookRotation, _root.transform.rotation);
          targetRotation.ToAngleAxis(out float degrees, out Vector3 axis);
          axis.Normalize();
          float radians = degrees * Mathf.Deg2Rad;

          _root.AddTorque(axis * (radians * _stateConfig.UpperBody.Grab.ClimbingRotationForce * 1000));

          Quaternion GetShortestRotation(Quaternion a, Quaternion b)
          {
            return Quaternion.Dot(a, b) < 0
              ? a * Quaternion.Inverse(Multiply(b, -1))
              : a * Quaternion.Inverse(b);

            Quaternion Multiply(Quaternion input, float scalar) =>
              new Quaternion(input.x * scalar, input.y * scalar, input.z * scalar, input.w * scalar);
          }
        }
      }
    }

    private void ClimbingSwitchAnimation()
    {
      if (!CheckGrounded())
      {
        Switch(ConnectionType.RightArm, _input.GetKey(KeyCode.Mouse1), _animationLayerClimbingR, ref _climbingLayerWeightR);
        Switch(ConnectionType.LeftArm, _input.GetKey(KeyCode.Mouse0), _animationLayerClimbingL, ref _climbingLayerWeightL);
      }
      else
        ClimbingResetAnimation();

      void Switch(ConnectionType connectionType, bool armInput, int animLayer, ref float animWeight)
      {
        if (_input.GetKey(KeyCode.E))
        {
          if (armInput)
          {
            animWeight = Mathf.Lerp(animWeight, 0, _stateConfig.UpperBody.Grab.ToClimbingTransitionSpeed * Time.deltaTime);
            SetAnimationLayerWeight(animLayer, animWeight);
          }
          else
          {
            if (ConnectionControllerIsConnected(connectionType) && ClimbingCheckInput())
            {
              animWeight = Mathf.Lerp(animWeight, 1, _stateConfig.UpperBody.Grab.ToClimbingTransitionSpeed * Time.deltaTime);
              SetAnimationLayerWeight(animLayer, animWeight);

              PlayAnimation(_stateConfig.UpperBody.Grab.ClimbAnimState, animLayer);
            }
            else
            {
              animWeight = Mathf.Lerp(animWeight, 0, _stateConfig.UpperBody.Grab.ToClimbingTransitionSpeed * Time.deltaTime);
              SetAnimationLayerWeight(animLayer, animWeight);
            }
          }
        }
      }
    }

    private void ClimbingResetAnimation()
    {
      _climbingLayerWeightR = _climbingLayerWeightL = 0;

      SetAnimationLayerWeight(_animationLayerClimbingR, _climbingLayerWeightR);
      SetAnimationLayerWeight(_animationLayerClimbingL, _climbingLayerWeightL);
    }

    private void ClimbingSwitchConnection()
    {
      Switch(ConnectionType.RightArm, _input.GetKey(KeyCode.Mouse1));
      Switch(ConnectionType.LeftArm, _input.GetKey(KeyCode.Mouse0));

      void Switch(ConnectionType connectionType, bool armInput)
      {
        if (_input.GetKey(KeyCode.E) && armInput)
          DeactivateConnectionController(connectionType);
        else if (_input.GetKey(KeyCode.E) && !armInput)
          ActivateConnectionController(_stateConfig.UpperBody.Grab);
      }
    }

    private bool ClimbingCheckInput() =>
      _input.GetKey(KeyCode.Space) && (_climbingUpDelay >= _stateConfig.UpperBody.Grab.ClimbingUpDelay || AnyConnectionIsKinematics());

    private bool AnyConnectionIsKinematics()
    {
      Rigidbody r_rigidbody = ConnectionControllerGetConnection(ConnectionType.RightArm);
      Rigidbody l_rigidbody = ConnectionControllerGetConnection(ConnectionType.RightArm);

      return r_rigidbody && r_rigidbody.isKinematic || l_rigidbody && l_rigidbody.isKinematic;
    }
  }
}