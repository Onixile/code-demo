using System;
using UnityEngine;

namespace Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.StateConfigData
{
  [Serializable]
  public partial class ConditionData
  {
    public MuscleData Active;
    public TimingData Knockout;
  }
  
  [Serializable]
  public partial class MuscleData
  {
    public bool MusclesIsActive = true;
  }
    
  [Serializable]
  public partial class TimingData : MuscleData
  {
    [Range(0, 100)] public float Duration = 3;
  }
}
