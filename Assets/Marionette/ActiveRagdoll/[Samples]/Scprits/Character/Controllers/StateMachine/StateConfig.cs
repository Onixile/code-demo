using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.StateConfigData;
using UnityEngine;

namespace Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine
{
  [CreateAssetMenu(menuName = "Marionette/Active Ragdoll/States Config", fileName = "ActiveRagdoll-Config-States", order = 0)]
  public partial class StateConfig : ScriptableObject
  {
    public UpperBodyData UpperBody;
    public LowerBodyData LowerBody;
    public ConditionData Condition;
    public CheckerData Checker;
  }
}