using System;
using System.Collections.Generic;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.Behaviour.UB;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.Checker;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.StateMachines;
using Marionette.ActiveRagdoll.Runtime.Animation._Interfaces;

namespace Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.Base
{
  public abstract class BaseCondition : State, IConditionState
  {
    protected bool _behaviourIsActive;

    private readonly BehaviourStateMachine _upperBodyStates;
    private readonly BehaviourStateMachine _lowerBodyStates;
    private readonly ConditionStateMachine _conditionStateMachine;
    private readonly IAnimationController _animationController;
    private readonly Grounded _groundedChecker;

    protected BaseCondition(StateConfig stateConfig, IAnimationController animationController,
      BehaviourStateMachine upperBodyStates, BehaviourStateMachine lowerBodyStates, ConditionStateMachine conditionStateMachine,
      Dictionary<Type, IUpdatableState> checkerStates) : base(stateConfig, checkerStates)
    {
      _animationController = animationController;
      _upperBodyStates = upperBodyStates;
      _lowerBodyStates = lowerBodyStates;
      _conditionStateMachine = conditionStateMachine;
      _groundedChecker = GetCheckerState<Grounded>();
    }

    public virtual void Enter()
    {
      _behaviourIsActive = true;
      _animationController.ConnectionController.Deactivate();
    }

    public virtual void Exit() => 
      _behaviourIsActive = false;

    public virtual void BackgroundUpdate()
    {
    }

    public virtual void Update()
    {
      CheckCondition();

      if (_behaviourIsActive)
      {
        _upperBodyStates.Update();
        _lowerBodyStates.Update();
        
        RecalculateInertiaTensor();
      }
    }

    public virtual void LateUpdate()
    {
      if (_behaviourIsActive)
      {
        _upperBodyStates.LateUpdate();
        _lowerBodyStates.LateUpdate();
      }
    }

    public virtual void FixedUpdate()
    {
      if (_behaviourIsActive)
      {
        _upperBodyStates.FixedUpdate();
        _lowerBodyStates.FixedUpdate();
      }
    }

    public virtual void OnDrawGizmos()
    {
      if (_behaviourIsActive)
      {
        _upperBodyStates.OnDrawGizmos();
        _lowerBodyStates.OnDrawGizmos();
      }
    }

    protected void EnterToIdleBehaviourState()
    {
      _upperBodyStates.Enter<Idle>();
      _lowerBodyStates.Enter<Behaviour.LB.Steer>();
    }

    protected abstract void CheckCondition();
    
    protected virtual void GoToState<TState>() where TState : class, IState =>
      _conditionStateMachine.Enter<TState>();

    protected virtual void SetActiveMuscles(bool value) => 
      _animationController.MusclesController.SetActiveMuscles(value);

    private void RecalculateInertiaTensor() => 
      _animationController.RecalculateInertiaTensor(_groundedChecker.Result);
  }
}