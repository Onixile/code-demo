using System;
using System.Collections.Generic;
using Marionette.ActiveRagdoll.Runtime._Interfaces;

namespace Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.StateMachines
{
  public abstract class StateMachine : IUpdate, IDrawGizmos
  {
    protected Dictionary<Type, IExitableState> _states;
    protected IExitableState _activeState;

    public void SetStates<TDefaultState>(Dictionary<Type, IExitableState> states) where TDefaultState : class, IState
    {
      _states = states;
      Enter<TDefaultState>();
    }

    public void Enter<TState>() where TState : class, IState
    {
      IState state = ChangeState<TState>();
      state.Enter();
    }

    public void Enter<TState, TPayload>(TPayload payload) where TState : class, IPayloadedState<TPayload>
    {
      IPayloadedState<TPayload> state = ChangeState<TState>();
      state.Enter(payload);
    }

    public bool CurrentStateIs(Type type) => 
      _activeState.GetType() == type;

    public abstract void Update();
    
    public abstract void LateUpdate();

    public abstract void FixedUpdate();

    public abstract void OnDrawGizmos();

    private TState ChangeState<TState>() where TState : class, IExitableState
    {
      _activeState?.Exit();
      var state = GetState<TState>();
      _activeState = state;

      return state;
    }

    private TState GetState<TState>() where TState : class, IExitableState =>
      _states[typeof(TState)] as TState;
  }
}