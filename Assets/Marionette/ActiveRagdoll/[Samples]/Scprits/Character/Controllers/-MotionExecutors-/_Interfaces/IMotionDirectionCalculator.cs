using UnityEngine;

namespace Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers._MotionExecutors_._Interfaces
{
  public interface IMotionDirectionCalculator
  {
    Vector3 Get();
  }
}