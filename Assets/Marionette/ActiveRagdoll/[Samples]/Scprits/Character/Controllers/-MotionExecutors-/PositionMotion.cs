using System;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers._MotionExecutors_._Interfaces;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.StateConfigData;
using Marionette.ActiveRagdoll.Runtime._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Joint;
using UnityEngine;

namespace Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers._MotionExecutors_
{
  public class PositionMotion : IUpdate, IMotionSleep, IDrawGizmos
  {
    private readonly IMotionDirectionCalculator _motionDirectionCalculator;
    private readonly JointDatasContainer _joints;
    private readonly Func<bool> _onGetIsGrounded;
    private readonly Rigidbody _rigidbody;

    private SteeringData.Params _steeringData;
    private Vector3 _motionDirection;
    private bool _isSleeping;

    public PositionMotion(IMotionDirectionCalculator motionDirectionCalculator, JointDatasContainer jointDatasContainer,
      Func<bool> onGetIsGrounded)
    {
      _motionDirectionCalculator = motionDirectionCalculator;
      _joints = jointDatasContainer;
      _onGetIsGrounded = onGetIsGrounded;

      _rigidbody = jointDatasContainer.Get(JointType.Pelvis).Rigidbody;
      ResetDirections();
    }

    public void Sleep(bool value) =>
      _isSleeping = value;

    public void ResetDirections() =>
      _motionDirection = Vector3.Cross(_rigidbody.transform.right, Vector3.up);

    public void SetSteeringData(SteeringData.Params steeringData) =>
      _steeringData = steeringData;

    public void Update() =>
      SetDirection();

    public void LateUpdate()
    {
    }

    public void FixedUpdate()
    {
      if (_onGetIsGrounded.Invoke())
        Move();
    }

    public void OnDrawGizmos()
    {
      Color gizmo = Gizmos.color;
      Gizmos.color = Color.white;
      Gizmos.DrawRay(_rigidbody.transform.position, _motionDirection.normalized * 5);
      Gizmos.color = gizmo;
    }

    private void SetDirection()
    {
      _motionDirection = _motionDirectionCalculator.Get();
      _motionDirection.y = 0;
    }

    private void Move()
    {
      Vector3 velocity = GetAveragedVelocity();
      velocity.y = _motionDirection.y;

      float ratio = 1 - Vector3.Angle(Vector3.down, -_rigidbody.transform.up) / 180;
      ratio = Mathf.Lerp(ratio, 1, Mathf.Abs(_steeringData.UpsideDownDirection));

      Vector3 forceDirection;

      if (_isSleeping)
        forceDirection = Vector3.zero;
      else
      {
        forceDirection = _motionDirection;
        forceDirection.Normalize();
      }

      Vector3 force = forceDirection * _steeringData.PositionMotion.Force * ratio;
      Vector3 damping = Vector3.ClampMagnitude(velocity * _steeringData.PositionMotion.Smoother, force.magnitude);
      force -= damping;

      _rigidbody.AddForce(force, ForceMode.VelocityChange);

      AddFriction(velocity);
    }

    private void AddFriction(Vector3 velocity)
    {
      float friction = _isSleeping || _motionDirection == Vector3.zero
        ? _steeringData.PositionMotion.Friction.Static
        : _steeringData.PositionMotion.Friction.Active;
      
      _rigidbody.AddForce(-velocity.normalized * friction, ForceMode.VelocityChange);
    }

    private Vector3 GetAveragedVelocity()
    {
      Vector3 vel = Vector3.zero;
      int amount = 0;

      foreach (JointData joint in _joints.GetAll())
      {
        if (joint.Rigidbody)
        {
          vel += joint.Rigidbody.velocity;
          amount++;
        }
      }

      return vel / amount;
    }
  }
}