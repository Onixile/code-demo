using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers._MotionExecutors_._Interfaces;
using UnityEngine;

namespace Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers._MotionExecutors_.DirectionCalculators
{
  public class MotionDirectionToTargetCalculator : IMotionDirectionCalculator
  {
    private readonly Transform _origin;
    private readonly Transform _target;

    public MotionDirectionToTargetCalculator(Transform origin, Transform target)
    {
      _origin = origin;
      _target = target;
    }

    public Vector3 Get()
    {
      if (_target == null || _origin == null)
      {
        Logging();
        return Vector3.zero;
      }

      return (_target.position - _origin.position).normalized;
    }

    private void Logging()
    {
      string log = $"(!!!) Some data is null - [origin is null: {_origin == null}] or [target is null: {_target == null}]";
      
      Debug.Log(log);
      Debug.LogWarning(log);
    }
  }
}