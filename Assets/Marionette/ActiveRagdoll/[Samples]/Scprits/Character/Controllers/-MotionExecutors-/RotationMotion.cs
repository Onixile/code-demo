using System;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers._MotionExecutors_._Interfaces;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine.StateConfigData;
using Marionette.ActiveRagdoll.Runtime._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Joint;
using UnityEngine;

namespace Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers._MotionExecutors_
{
  public class RotationMotion : IUpdate, IMotionSleep, IDrawGizmos
  {
    public bool LookAtTarget { get; set; }
    public Vector3 LookDirection => _currentDirection;
    
    private readonly IMotionDirectionCalculator _motionDirectionCalculator;
    private readonly Func<bool> _onGetIsGrounded;
    private readonly Func<Vector3> _onGetLookAtTargetPosition;

    private readonly Rigidbody _rigidbody;
    
    private SteeringData.Params _steeringData;
    private Vector3 _motionDirection;
    private Vector3 _currentDirection;

    private bool _isSleeping;
    
    public RotationMotion(IMotionDirectionCalculator motionDirectionCalculator, JointDatasContainer jointDatasContainer, 
      Func<bool> onGetIsGrounded, Func<Vector3> onGetLookAtTargetPosition)
    {
      _motionDirectionCalculator = motionDirectionCalculator;
      _onGetIsGrounded = onGetIsGrounded;
      _onGetLookAtTargetPosition = onGetLookAtTargetPosition;

      _rigidbody = jointDatasContainer.Get(JointType.Pelvis).Rigidbody;
      
      ResetDirections();
    }

    public void Sleep(bool value) => 
      _isSleeping = value;
    
    public void ResetDirections() => 
      _motionDirection = _currentDirection = Vector3.Cross(_rigidbody.transform.right, Vector3.up);

    public void SetSteeringData(SteeringData.Params steeringData) =>
      _steeringData = steeringData;

    public void Update() => 
      SetDirection();

    public void LateUpdate()
    {
    }

    public void FixedUpdate()
    {
      if (_onGetIsGrounded.Invoke())
        Rotate();
    }

    public void OnDrawGizmos()
    {
      Color gizmo = Gizmos.color;
      Gizmos.color = LookAtTarget ? Color.yellow : Color.white;
      Gizmos.DrawRay(_rigidbody.transform.position, _currentDirection.normalized * 3);
      Gizmos.color = gizmo;
    }

    private void SetDirection()
    {
      _motionDirection = LookAtTarget
        ? (_onGetLookAtTargetPosition.Invoke() - _rigidbody.transform.position).normalized
        : _motionDirectionCalculator.Get();
      _motionDirection.y = 0;
    }

    private void Rotate()
    {
      if (_isSleeping) 
      {
        _currentDirection = _rigidbody.transform.forward;
        _currentDirection.y = 0;
      }
      else
      {
        Vector3 targetDirection = _motionDirection == Vector3.zero ? _currentDirection : _motionDirection;
        _currentDirection = Vector3.Lerp(_currentDirection, targetDirection, Time.fixedDeltaTime * _steeringData.RotationMotion.Smoother); 
      }
      
      Quaternion lookRotation = _steeringData.UpsideDownDirection == 0
        ? Quaternion.LookRotation(_currentDirection)
        : Quaternion.LookRotation(_currentDirection.normalized * 0.005f +
                                  (_steeringData.UpsideDownDirection > 0 ? Vector3.up : Vector3.down));

      Quaternion targetRotation = GetShortestRotation(lookRotation, _rigidbody.transform.rotation);
      targetRotation.ToAngleAxis(out float degrees, out Vector3 axis);
      axis.Normalize();
      float radians = degrees * Mathf.Deg2Rad;

      float ratio = 1 - Vector3.Angle(Vector3.down, -_rigidbody.transform.up) / 180;
      ratio = Mathf.Lerp(ratio, 1, Mathf.Abs(_steeringData.UpsideDownDirection));

      _rigidbody.AddTorque(axis * (radians * _steeringData.RotationMotion.Force * 1000 * ratio));

      AddFriction(_rigidbody.angularVelocity);

      Quaternion GetShortestRotation(Quaternion a, Quaternion b)
      {
        return Quaternion.Dot(a, b) < 0
          ? a * Quaternion.Inverse(Multiply(b, -1))
          : a * Quaternion.Inverse(b);

        Quaternion Multiply(Quaternion input, float scalar) =>
          new Quaternion(input.x * scalar, input.y * scalar, input.z * scalar, input.w * scalar);
      }
    }
    
    private void AddFriction(Vector3 velocity)
    {
      float friction = _isSleeping || _motionDirection == Vector3.zero
        ? _steeringData.RotationMotion.Friction.Static
        : _steeringData.RotationMotion.Friction.Active;
      
      _rigidbody.AddTorque(-velocity * (friction / 100)); 
    }
  }
}