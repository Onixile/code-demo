using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers._MotionExecutors_._Interfaces;
using Marionette.ActiveRagdoll.Runtime._Interfaces;
using UnityEngine;

namespace Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers._MotionExecutors_.DirectionCalculators
{
  public class InputMotionDirectionCalculator : IMotionDirectionCalculator
  {
    private readonly IInput _input;
    private readonly ICamera _camera;

    public InputMotionDirectionCalculator(IInput input, ICamera camera)
    {
      _input = input;
      _camera = camera;
    }

    public Vector3 Get()
    {
      Vector3 axis = new Vector3(_input.GetAxis().x, 0f, _input.GetAxis().y);
      Vector3 direction = Vector3.zero;

      Transform cameraTransform = _camera.GetTransform();

      if (axis.z > 0)
        direction += cameraTransform.forward;
      if (axis.x > 0)
        direction += cameraTransform.right;
      if (axis.x < 0)
        direction += -cameraTransform.right;
      if (axis.z < 0)
        direction += -cameraTransform.forward;

      return direction;
    }
  }
}