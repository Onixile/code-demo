using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers._MotionExecutors_._Interfaces;
using UnityEngine;

namespace Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers._MotionExecutors_.DirectionCalculators
{
  public class ZeroMotionDirectionCalculator : IMotionDirectionCalculator
  {
    public Vector3 Get() =>
      Vector3.zero;
  }
}