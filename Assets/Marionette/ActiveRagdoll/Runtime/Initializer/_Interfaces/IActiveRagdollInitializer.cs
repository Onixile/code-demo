using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine;
using Marionette.ActiveRagdoll.Runtime._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Animation;
using Marionette.ActiveRagdoll.Runtime.Animation._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Animation.InverseKinematics;
using Marionette.ActiveRagdoll.Runtime.Equipment;
using Marionette.ActiveRagdoll.Runtime.Equipment._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Joint;
using UnityEngine;

namespace Marionette.ActiveRagdoll.Runtime.Initializer._Interfaces
{
  public interface IActiveRagdollInitializer
  {
    AnimationConfig AnimationConfig { get; }
    StateConfig BehaviourConfig { get; }
    EquipmentConfig EquipmentConfig { get; }

    void SetupInput(out IInput input);
    
    void SetupCamera(out ICamera camera, JointDatasContainer jointDatasContainer);
    
    void SetupSelfCollision(out ISelfCollisionController collisionController, JointDatasContainer jointDatasContainer);

    void SetupAnimationControllingPoints(out AnimationControllingPointsContainer animationControllingPoints, out Animator animator,
      AnimationConfig animationConfig, JointDatasContainer jointDatasContainer, Transform parent);

    IEquipmentController InitializeEquipmentController(GameObject parent, EquipmentConfig equipmentConfig, JointDatasContainer jointDatasContainer);

    ICharacterController InitializeCharacterController(IInput input, ICamera camera, 
      StateConfig stateConfig, IAnimationController animationController, JointDatasContainer jointsContainer);

    IAnimationController InitializeAnimationController(ActiveRagdoll owner, AnimationConfig animationConfig, Animator animator,
      JointDatasContainer jointDatasContainer, AnimationControllingPointsContainer inverseKinematicsPointsContainer, ISelfCollisionController collisionController, IEquipmentController equipmentController);
  }
}