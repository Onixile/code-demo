using System.Collections.Generic;
using System.Linq;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Camera;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Character.Controllers.StateMachine;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Equipment;
using Marionette.ActiveRagdoll._Samples_._General_.Scprits.Input;
using Marionette.ActiveRagdoll.Runtime._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Animation;
using Marionette.ActiveRagdoll.Runtime.Animation._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Animation.Connection;
using Marionette.ActiveRagdoll.Runtime.Animation.InverseKinematics;
using Marionette.ActiveRagdoll.Runtime.Animation.Miscellaneous;
using Marionette.ActiveRagdoll.Runtime.Animation.Muscle;
using Marionette.ActiveRagdoll.Runtime.Equipment;
using Marionette.ActiveRagdoll.Runtime.Equipment._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Initializer._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Joint;
using Marionette.ActiveRagdoll.Utility;
using UnityEngine;
using UnityEngine.Animations;

namespace Marionette.ActiveRagdoll.Runtime.Initializer
{
  public abstract class ActiveRagdollInitializer : MonoBehaviour, IActiveRagdollInitializer
  {
    public AnimationConfig AnimationConfig => _animationConfig;
    public StateConfig BehaviourConfig => _behaviourConfig;
    public EquipmentConfig EquipmentConfig => _equipmentConfig;

    [SerializeField] private AnimationConfig _animationConfig;
    [SerializeField] private StateConfig _behaviourConfig;
    [SerializeField] private EquipmentConfig _equipmentConfig;

    public virtual void SetupInput(out IInput input) =>
      input = new SimpleInput();

    public virtual void SetupCamera(out ICamera camera, JointDatasContainer jointDatasContainer)
    {
      camera = FindObjectOfType<SimpleCamera>();
      camera.SetTarget(jointDatasContainer.Get(JointType.Pelvis).Transform);
    }

    public virtual void SetupSelfCollision(out ISelfCollisionController collisionController, JointDatasContainer jointDatasContainer)
    {
      collisionController = new SelfCollisionController(jointDatasContainer.GetAll());
      collisionController.SetIgnoreSelfCollision(true);
    }

    public virtual void SetupAnimationControllingPoints(out AnimationControllingPointsContainer animationControllingPoints, out Animator animator,
      AnimationConfig animationConfig, JointDatasContainer jointDatasContainer, Transform parent)
    {
      animationControllingPoints = Instantiate(animationConfig.Prefabs.RagdollControllingPoints, parent).gameObject.GetComponent<AnimationControllingPointsContainer>();
      animationControllingPoints.gameObject.name = animationControllingPoints.gameObject.name.Replace("(Clone)", string.Empty);

      animator = animationControllingPoints.gameObject.GetComponent<Animator>();

      AddConstrain(animationControllingPoints.R_Arm.PointsParent, jointDatasContainer.Get(JointType.Chest).Transform);
      AddConstrain(animationControllingPoints.L_Arm.PointsParent, jointDatasContainer.Get(JointType.Chest).Transform);
      AddConstrain(animationControllingPoints.R_Leg.PointsParent, jointDatasContainer.Get(JointType.Pelvis).Transform);
      AddConstrain(animationControllingPoints.L_Leg.PointsParent, jointDatasContainer.Get(JointType.Pelvis).Transform);
      AddConstrain(animationControllingPoints.Body.UpperParent, jointDatasContainer.Get(JointType.Pelvis).Transform);
      AddConstrain(animationControllingPoints.Body.LowerParent, jointDatasContainer.Get(JointType.Pelvis).Transform);

      void AddConstrain(Transform target, Transform source)
      {
        ParentConstraint constraint = target.gameObject.AddComponent<ParentConstraint>();
        ConstraintSource constraintSource = new ConstraintSource();
        constraintSource.sourceTransform = source;
        constraintSource.weight = 1;
        constraint.AddSource(constraintSource);
        constraint.constraintActive = true;
      }
    }

    public virtual IEquipmentController InitializeEquipmentController(GameObject owner,
      EquipmentConfig equipmentConfig, JointDatasContainer jointDatasContainer)
    {
      IEquipmentController equipmentController = new EquipmentController(owner, jointDatasContainer);
      equipmentController.SetEquipment(equipmentConfig);

      return equipmentController;
    }

    public abstract ICharacterController InitializeCharacterController(IInput input, ICamera camera,
      StateConfig stateConfig, IAnimationController animationController, JointDatasContainer jointsContainer);

    public virtual IAnimationController InitializeAnimationController(ActiveRagdoll owner, AnimationConfig animationConfig, Animator animator,
      JointDatasContainer jointDatasContainer, AnimationControllingPointsContainer inverseKinematicsPointsContainer,
      ISelfCollisionController collisionController, IEquipmentController equipmentController)
    {
      JointData[] jointDatas = jointDatasContainer.GetAll();
      IMusclesController musclesController = new MusclesController(jointDatas, collisionController);

      SetupInverseKinematicsLimbControllers(out List<IInverseKinematicsLimbController> inverseKinematicsLimbControllers,
        inverseKinematicsPointsContainer, jointDatas);
      SetupBendTargetControllers(out List<IInverseKinematicsBendTargetController> bendTargetControllers,
        animationConfig, inverseKinematicsPointsContainer, jointDatas);
      SetupInverseKinematicsLookAtController(out IInverseKinematicsLookAtController inverseKinematicsLookAtController, inverseKinematicsPointsContainer, jointDatas);

      SetupConnectionInitializationDatas(out ConnectionInitializationDatas connectionInitializationDatas, jointDatasContainer, equipmentController);
      MusclesAnimation musclesAnimation = inverseKinematicsPointsContainer.GetComponent<MusclesAnimation>();
      musclesAnimation.Construct(animationConfig);

      ILegPointsUnderPelvisController legPointsUnderPelvisController = new LegPointsUnderPelvisController(jointDatasContainer, inverseKinematicsPointsContainer);
      IEyesLookAtDirectionController eyesLookAtDirectionController = new EyesLookAtDirectionController(jointDatasContainer, animationConfig, equipmentController);
      IBodyTargetPointController bodyTargetPointController = new BodyTargetPointController(animationConfig, owner, inverseKinematicsPointsContainer.Body.TargetPoint);
      IConnectionController connectionController = new ConnectionController(connectionInitializationDatas.Get());

      return new AnimationController(jointDatasContainer, animationConfig, animator,
        inverseKinematicsLimbControllers, inverseKinematicsLookAtController, bendTargetControllers, musclesController,
        legPointsUnderPelvisController, eyesLookAtDirectionController, bodyTargetPointController,
        musclesAnimation, connectionController);
    }

    protected virtual void SetupInverseKinematicsLimbControllers(out List<IInverseKinematicsLimbController> inverseKinematicsLimbControllers,
      AnimationControllingPointsContainer inverseKinematicsPointsContainer, JointData[] jointDatas)
    {
      inverseKinematicsLimbControllers = new List<IInverseKinematicsLimbController>();

      AddLimbIK(ref inverseKinematicsLimbControllers, inverseKinematicsPointsContainer.R_Arm.TargetPoint, inverseKinematicsPointsContainer.R_Arm.BendPoint, jointDatas,
        JointType.R_Arm, JointType.R_Elbow, JointType.R_Hand);
      AddLimbIK(ref inverseKinematicsLimbControllers, inverseKinematicsPointsContainer.R_Leg.TargetPoint, inverseKinematicsPointsContainer.R_Leg.BendPoint, jointDatas,
        JointType.R_Leg, JointType.R_Knee, JointType.R_Foot);
      AddLimbIK(ref inverseKinematicsLimbControllers, inverseKinematicsPointsContainer.L_Arm.TargetPoint, inverseKinematicsPointsContainer.L_Arm.BendPoint, jointDatas,
        JointType.L_Arm, JointType.L_Elbow, JointType.L_Hand);
      AddLimbIK(ref inverseKinematicsLimbControllers, inverseKinematicsPointsContainer.L_Leg.TargetPoint, inverseKinematicsPointsContainer.L_Leg.BendPoint, jointDatas,
        JointType.L_Leg, JointType.L_Knee, JointType.L_Foot);

      void AddLimbIK(ref List<IInverseKinematicsLimbController> IKLimbControllers, Transform rootTarget, Transform bendGoal, JointData[] masterJointDatas,
        JointType limbRoot, JointType limbBend, JointType limbEnd)
      {
        IInverseKinematicsLimbController inverseKinematicsController = new InverseKinematicsLimbController(rootTarget, bendGoal,
          new InverseKinematicsTransformDecorator(masterJointDatas.FirstOrDefault(x => x.Type == limbRoot)?.Transform, masterJointDatas.FirstOrDefault(x => x.Type == limbRoot)?.Joint),
          new InverseKinematicsTransformDecorator(masterJointDatas.FirstOrDefault(x => x.Type == limbBend)?.Transform, masterJointDatas.FirstOrDefault(x => x.Type == limbBend)?.Joint),
          new InverseKinematicsTransformDecorator(masterJointDatas.FirstOrDefault(x => x.Type == limbEnd)?.Transform));

        IKLimbControllers.Add(inverseKinematicsController);
      }
    }

    protected virtual void SetupBendTargetControllers(out List<IInverseKinematicsBendTargetController> bendTargetControllers,
      AnimationConfig animationConfig, AnimationControllingPointsContainer inverseKinematicsPointsContainer, JointData[] jointDatas)
    {
      bendTargetControllers = new List<IInverseKinematicsBendTargetController>();
      AddPoleTargetController(ref bendTargetControllers, jointDatas, AnimationDirectionType.Down, JointType.Chest,
        inverseKinematicsPointsContainer.R_Arm.TargetPoint, inverseKinematicsPointsContainer.R_Arm.BendPoint, JointType.R_Arm, JointType.R_Elbow,
        animationConfig.InverseKinematics);
      AddPoleTargetController(ref bendTargetControllers, jointDatas, AnimationDirectionType.Up, JointType.Chest,
        inverseKinematicsPointsContainer.L_Arm.TargetPoint, inverseKinematicsPointsContainer.L_Arm.BendPoint, JointType.L_Arm, JointType.L_Elbow,
        animationConfig.InverseKinematics);
      AddPoleTargetController(ref bendTargetControllers, jointDatas, AnimationDirectionType.Right, JointType.Pelvis,
        inverseKinematicsPointsContainer.R_Leg.TargetPoint, inverseKinematicsPointsContainer.R_Leg.BendPoint, JointType.R_Leg, JointType.R_Knee,
        animationConfig.InverseKinematics);
      AddPoleTargetController(ref bendTargetControllers, jointDatas, AnimationDirectionType.Right, JointType.Pelvis,
        inverseKinematicsPointsContainer.L_Leg.TargetPoint, inverseKinematicsPointsContainer.L_Leg.BendPoint, JointType.L_Leg, JointType.L_Knee,
        animationConfig.InverseKinematics);

      void AddPoleTargetController(ref List<IInverseKinematicsBendTargetController> controllers, JointData[] masterJointDatas, AnimationDirectionType directionType, JointType directionOwner,
        Transform ikTarget, Transform ikPole, JointType limbRoot, JointType limbCenter, AnimationConfig.InverseKinematicsData inverseKinematicsData)
      {
        IInverseKinematicsBendTargetController bendTargetController = new InverseKinematicsBendTargetController(limbCenter, directionType,
          masterJointDatas.FirstOrDefault(x => x.Type == directionOwner)?.Transform,
          ikTarget, ikPole,
          masterJointDatas.FirstOrDefault(x => x.Type == limbRoot)?.Transform,
          masterJointDatas.FirstOrDefault(x => x.Type == limbCenter)?.Transform,
          inverseKinematicsData);

        controllers.Add(bendTargetController);
      }
    }

    protected virtual void SetupInverseKinematicsLookAtController(out IInverseKinematicsLookAtController inverseKinematicsLookAtController,
      AnimationControllingPointsContainer inverseKinematicsPointsContainer, JointData[] jointDatas)
    {
      inverseKinematicsLookAtController = new InverseKinematicsLookAtController(inverseKinematicsPointsContainer.Body.LookAtPoint, jointDatas.FirstOrDefault(x => x.Type == JointType.Pelvis)?.Transform,
        new[]
        {
          new InverseKinematicsTransformDecorator(jointDatas.FirstOrDefault(x => x.Type == JointType.Back)?.Transform, jointDatas.FirstOrDefault(x => x.Type == JointType.Back)?.Joint),
          new InverseKinematicsTransformDecorator(jointDatas.FirstOrDefault(x => x.Type == JointType.Chest)?.Transform, jointDatas.FirstOrDefault(x => x.Type == JointType.Chest)?.Joint)
        },
        new InverseKinematicsTransformDecorator(jointDatas.FirstOrDefault(x => x.Type == JointType.Head)?.Transform, jointDatas.FirstOrDefault(x => x.Type == JointType.Head)?.Joint));
    }

    protected virtual void SetupConnectionInitializationDatas(out ConnectionInitializationDatas connectionInitializationDatas,
      JointDatasContainer _jointDatasContainer, IEquipmentController equipmentController)
    {
      connectionInitializationDatas = new ConnectionInitializationDatas(new[]
        {
          new ConnectionInitializationData(ConnectionType.RightArm, _jointDatasContainer.Get(JointType.R_Hand)?.Transform,
            equipmentController.Get<EquipmentContainerHand>(JointType.R_Hand)?.ConnectionCollider,
            _jointDatasContainer.Get(JointType.R_Elbow), delegate(float breakForce) { Debug.Log($"OnBreak: RightArm, breakForce:{breakForce}"); }
          ),

          new ConnectionInitializationData(ConnectionType.LeftArm, _jointDatasContainer.Get(JointType.L_Hand)?.Transform,
            equipmentController.Get<EquipmentContainerHand>(JointType.L_Hand)?.ConnectionCollider,
            _jointDatasContainer.Get(JointType.L_Elbow), delegate(float breakForce) { Debug.Log($"OnBreak: LeftArm, breakForce:{breakForce}"); }
          )
        }
      );
    }
  }
}