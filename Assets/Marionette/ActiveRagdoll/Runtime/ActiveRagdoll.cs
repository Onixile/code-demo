using Marionette.ActiveRagdoll._Samples_._General_.Utility.Display_Info.Scripts;
using Marionette.ActiveRagdoll._Samples_._Main_Demo_.Initializer;
using Marionette.ActiveRagdoll.Runtime._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Animation;
using Marionette.ActiveRagdoll.Runtime.Animation._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Animation.Connection;
using Marionette.ActiveRagdoll.Runtime.Equipment._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Initializer;
using Marionette.ActiveRagdoll.Runtime.Initializer._Interfaces;
using Marionette.ActiveRagdoll.Runtime.Joint;
using UnityEngine;

namespace Marionette.ActiveRagdoll.Runtime
{
  [RequireComponent(typeof(JointDatasContainer))]
  public class ActiveRagdoll : MonoBehaviour
  {
    protected ICharacterController _characterController;
    protected IAnimationController _animationController;
    protected IEquipmentController _equipmentController;

    protected virtual void Awake()
    {
      Initialization();
      InitializeAdditionalOptions();
      AddEvents();
    }

    protected virtual void Initialization()
    {
      IActiveRagdollInitializer initializer = GetComponent<ActiveRagdollInitializer>() ?? gameObject.AddComponent<MainDemoInitializer>();

      JointDatasContainer jointDatasContainer = GetComponent<JointDatasContainer>();

      initializer.SetupInput(out IInput input);
      initializer.SetupCamera(out ICamera camera, jointDatasContainer);
      initializer.SetupSelfCollision(out ISelfCollisionController collisionController, jointDatasContainer);
      initializer.SetupAnimationControllingPoints(out AnimationControllingPointsContainer animationControllingPoints,
        out Animator animator, initializer.AnimationConfig, jointDatasContainer, transform);

      _equipmentController = initializer.InitializeEquipmentController(gameObject, initializer.EquipmentConfig, jointDatasContainer);
      _animationController = initializer.InitializeAnimationController(this, initializer.AnimationConfig, animator,
        jointDatasContainer, animationControllingPoints, collisionController, _equipmentController);
      _characterController = initializer.InitializeCharacterController(input, camera, initializer.BehaviourConfig, _animationController, jointDatasContainer);
    }

    protected virtual void InitializeAdditionalOptions()
    {
      if (_animationController is { BodyTargetPointController: { } })
      {
        JointDatasContainer jointDatasContainer = GetComponent<JointDatasContainer>();
        GetComponentInChildren<DisplayInfoLookAt>()?.Initialization(_animationController.BodyTargetPointController, jointDatasContainer.Get(JointType.Pelvis).Transform);
      }
    }

    protected virtual void AddEvents()
    {
      AddAnimationEvents();
      
      void AddAnimationEvents()
      {
        if (_animationController != null)
        {
          if (_animationController.MusclesController != null)
            _animationController.MusclesController.OnSetActiveMuscles += delegate(bool value) { Debug.Log($"OnSetActiveMuscles: {value}"); };

          if (_animationController.BodyTargetPointController != null)
          {
            _animationController.BodyTargetPointController.OnActivate += delegate { Debug.Log("BodyTargetPointController: OnActivate"); };
            _animationController.BodyTargetPointController.OnDeactivate += delegate { Debug.Log("BodyTargetPointController: OnDeactivate"); };
          }

          if (_animationController.ConnectionController != null)
          {
            _animationController.ConnectionController.OnConnect += delegate(ConnectionType value) { Debug.Log($"ConnectionController: OnConnect-{value.ToString()}"); };
            _animationController.ConnectionController.OnDisconnect += delegate(ConnectionType value) { Debug.Log($"ConnectionController: OnDisconnect-{value.ToString()}"); };
          }
        }
      }
    }

    protected virtual void Update()
    {
      _characterController?.Update();
      _animationController?.Update();
    }

    protected virtual void LateUpdate()
    {
      _characterController?.LateUpdate();
      _animationController?.LateUpdate();
    }

    protected virtual void FixedUpdate()
    {
      _characterController?.FixedUpdate();
      _animationController?.FixedUpdate();
    }

    protected virtual void OnDrawGizmos()
    {
      if (Application.isPlaying)
      {
        _characterController?.OnDrawGizmos();
        _animationController?.OnDrawGizmos();
      }
    }
  }
}